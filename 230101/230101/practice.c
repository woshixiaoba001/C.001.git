#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//描述
//小乐乐的班级进行了一次期中考试，考试一共有3门科目：数学，语文，英语，小乐乐的班主任决定给没有通过考核的同学家长开一次家长会，考核的标准是三科平均分不低于60分，所以现在想请你帮忙算一算小乐乐会不会被叫家长。
//输入描述：
//一行，输入三个整数（表示小乐乐的数学、语文、英语的成绩），用空格分隔。
//输出描述：
//一行，如果小乐乐会被请家长则输出“YES”，否则输出“NO”。
int main() {
    int math, yuwen, english;
    scanf("%d%d%d", &math, &yuwen, &english);
    int ave = (math + yuwen + english) / 3;
    if (ave >= 60)
        printf("%s", "NO");
    else
        printf("%s", "YES");
    return 0;
}