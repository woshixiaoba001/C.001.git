#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
#include <vector>
using namespace std;

string multiply(string num1, string num2) {
    int len1 = num1.size();
    int len2 = num2.size();
    vector<int> dot;
    dot.resize(len1 + len2, 0);

    string A(num1);
    string B(num2);
    reverse(A.begin(), A.end());
    reverse(B.begin(), B.end());

    for (int i = 0; i < A.size(); i++)
    {
        int tmp1 = A[i] - '0';
        for (int j = 0; j < B.size(); j++)
        {
            int tmp2 = B[j] - '0';
            dot[i + j] += tmp1 * tmp2;
        }
    }

    int next = 0;
    for (int i = 0; i < dot.size(); i++)
    {
        if ((dot[i] + next) / 10 > 0)
        {
            int tmp_dot = dot[i];
            dot[i] = (tmp_dot + next) % 10;
            next = (tmp_dot+next)/ 10;
        }
        else
        {
            dot[i] = dot[i] + next;
            next = 0;
        }
    }
    if (dot[dot.size() - 1] == 0)
        dot.resize(len1 + len2 - 1);
    string ret;
    ret.reserve(dot.size());
    for (int i = 0; i < dot.size(); i++)
    {
        ret.push_back(dot[i] + '0');
    }
    if (ret.back() == '0')
        ret.resize(ret.size() - 1);
    reverse(ret.begin(), ret.end());
    
    return ret;
}

void test()
{
    string s1 = "123";
    string s2 = "456";

    cout << multiply(s1,s2);

}

int main()
{
    test();
    return 0;
}

