#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//判断一个数是否为奇数
//int main()
//{
//	int input = 0;
//	while (scanf("%d", &input) != EOF)
//	{
//		if (input % 2 != 0)
//			printf("odd\n");
//		else
//			printf("even\n");
//	}
//	return 0;
//}

//输出1-100之间的奇数
//int main()
//{
//	int i = 1;
//	for (i = 1; i <= 100; i = i + 2)
//	{
//		printf("%d\n", i);
//	}
//	return 0;
//}

#include <stdio.h>

int main()
{
    int c;
    puts("Enter text. Include a dot ('.') in a sentence to exit:");
    do {
        c = getchar();
        putchar(c);
    } while (c != '.');
    return 0;
}