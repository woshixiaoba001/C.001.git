#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <time.h>

using std::cout;
using std::endl;

//struct A { int a[10000]; };
// 引用返回
//A TestFunc1(A a) 
//{ 
//	return a; 
//}
//
//A& TestFunc2(A a) 
//{ 
//	return a; 
//}
//
//void TestReturnByRefOrValue()
//{
//	A a;
//
//	// 以值作为函数的返回值类型
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc1(a);
//	size_t end1 = clock();
//
//	// 以引用作为函数的返回值类型
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc2(a);
//	size_t end2 = clock();
//
//	// 计算两个函数运算完成之后的时间
//	cout << "TestFunc1 time:" << end1 - begin1 << endl;
//	cout << "TestFunc2 time:" << end2 - begin2 << endl;
//}
//
//int main()
//{
//	TestReturnByRefOrValue();
//
//	return 0;
//}

int& Add(int a, int b)
{
	int c = a + b;
	return c;
}

class A
{
public:
	A()
	{
		cout << "A()" << endl;
	}
	void print()
	{
		cout << _a << endl;
	}
	void exc(int x)
	{
		_a = x;
	}

private:
	int _a;
};

A func()
{
	A a;
	a.exc(1);
	return  A(a);
}
void Teststring2()
{
	std::string s;
	// 测试reserve是否会改变string中有效元素个数
	s.reserve(100);
	cout << s.size() << endl;
	cout << s.capacity() << endl;

	// 测试reserve参数小于string的底层空间大小时，是否会将空间缩小
	s.reserve(50);
	cout << s.size() << endl;
	cout << s.capacity() << endl;
}

int main()
{
	Teststring2();

	/*int& ret = Add(1, 2);
	Add(3, 4);
	cout << "Add(1, 2) is :" << ret << endl;
	printf("hello world\n");*/
	return 0;
}