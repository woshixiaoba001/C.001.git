#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

void swap(int* pm, int* pn)
{
    int tmp = 0;
    tmp = *pm;
    *pm = *pn;
    *pn = tmp;
}

int main()
{
    int m = 0;
    int n = 0;
    scanf("%d%d", &m, &n);
    swap(&m, &n);
    printf("%d %d", m, n);
}