﻿#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//KiKi实现一个简单计算器，实现两个数的“加减乘除”运算，用户从键盘输入算式“操作数1运算符操作数2”，计算并输出表达式的值，如果输入的运算符号不包括在（ + 、 - 、 * 、 / ）范围内，输出“Invalid operation!”。当运算符为除法运算，即“ / ”时。如果操作数2等于0.0，则输出“Wrong!Division by zero!”
//
//数据范围：字符串长度满足 3≤n≤50 3≤n≤50  ，保证运算符是一个char类型字符。
//输入描述：
//输入一行字符串，操作数1 + 运算符 + 操作数2 （其中合法的运算符包括： + 、 - 、 * 、 / ）。
//输出描述：
//输出为一行。
//
//如果操作数和运算符号均合法，则输出一个表达式，操作数1运算符操作数2 = 运算结果，各数小数点后均保留4位，数和符号之间没有空格。
//
//如果输入的运算符号不包括在（ + 、 - 、 * 、 / ）范围内，输出“Invalid operation!”。当运算符为除法运算，即“ / ”时。
//
//如果操作数2等于0.0，则输出“Wrong!Division by zero!”。
int main()
{
    double a = 0.0;
    double b = 0.0;
    char s = 0;
    double sum = 0;
    scanf("%lf%c%lf", &a, &s, &b);
    if (s == '+')
    {
        sum = a + b;
        printf("%.4lf%c%.4lf=%.4lf", a, s, b, sum);
    }
    else if (s == '-')
    {
        sum = a - b;
        printf("%.4lf%c%.4lf=%.4lf", a, s, b, sum);

    }
    else if (s == '*')
    {
        sum = a * b;
        printf("%.4lf%c%.4lf=%.4lf", a, s, b, sum);

    }
    else if (s == '/')
    {
        if (0 == b)
            printf("Wrong!Division by zero!");
        else
        {
            sum = a / b;
            printf("%.4lf%c%.4lf=%.4lf", a, s, b, sum);
        }
    }
    else
        printf("Invalid operation!");
    return 0;
}