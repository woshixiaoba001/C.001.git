#define _CRT_SECURE_NO_WARNINGS 1
        }
#include <stdio.h>



        int main()
        {
            int count = 10;
            int n = 0;
            int neg = 0;
            int pos = 0;
            while (count--)
            {
                scanf("%d", &n);
                if (n < 0)
                {
                    neg++;
                }
                else
                {
                    pos++;
                }
            }
            printf("positive:%d\n", pos);
            printf("negative:%d\n", neg);

            return 0;
        }