#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

typedef int STDataType;

typedef struct stack
{
	STDataType* arr;
	int capacity;
	int top;
}ST;

//��ʼ��ջ
void Stack_Initial(ST* pst);
//����ջ
void Stack_Destroy(ST* pst);
//ѹջ
void Stack_push(ST* pst, STDataType X);
//��ջ
void Stack_pop(ST* pst);
//ջ��Ԫ��
STDataType Stack_top(ST* pst);
//ջ�Ƿ�Ϊ��
bool is_stack_empty(ST* pst);
//ջ��Ԫ�ظ���
int stack_size(ST* pst);

