#define _CRT_SECURE_NO_WARNINGS 1
#include "stack.h"
//��ʼ��ջ
void Stack_Initial(ST* pst)
{
	assert(pst);
	pst->arr = NULL;
	pst->capacity = 0;
	pst->top = 0;
}
//����ջ
void Stack_Destroy(ST* pst)
{
	free(pst->arr);
	pst->arr = NULL;
	pst->capacity = 0;
	pst->top = 0;
}
//ѹջ
void Stack_push(ST* pst, STDataType X)
{
	assert(pst);
	STDataType* tmp = NULL;
	if (pst->capacity == pst->top)
	{
		pst->capacity = pst->capacity == 0 ? 4 : 2 * pst->capacity;
		tmp = (STDataType*)realloc(pst->arr, pst->capacity * sizeof(STDataType));
		if (tmp == NULL)
		{
			perror("realloc fail:");
			exit(-1);
		}
		pst->arr = tmp;
		pst->arr[pst->top] = X;
	}
	else
	{
		pst->arr[pst->top] = X;
	}
	pst->top++;
}
//��ջ
void Stack_pop(ST* pst)
{
	assert(pst);
	assert(!is_stack_empty(pst));
	pst->top--;
}
//ջ��Ԫ��
STDataType Stack_top(ST* pst)
{
	assert(pst);
	assert(!is_stack_empty(pst));
	return pst->arr[pst->top - 1];
}
//ջ�Ƿ�Ϊ��
bool is_stack_empty(ST* pst)
{
	assert(pst);
	return pst->top == 0;
}
//ջ��Ԫ�ظ���
int stack_size(ST* pst)
{
	assert(pst);
	return pst->top;
}