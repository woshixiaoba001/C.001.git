#define _CRT_SECURE_NO_WARNINGS 1
#include "stack.h"

void teststack1()
{
	ST stack;
	Stack_Initial(&stack);

	Stack_push(&stack, 1);
	Stack_push(&stack, 2);
	Stack_push(&stack, 3);
	Stack_push(&stack, 4);
	Stack_push(&stack, 5);
	Stack_push(&stack, 6);
	printf("%d\n", is_stack_empty(&stack));
	printf("%d\n", stack_size(&stack));

	while (!is_stack_empty(&stack))
	{
		printf("%d ",Stack_top(&stack));
		Stack_pop(&stack);
	}
	printf("\n");

	printf("%d\n",is_stack_empty(&stack));
	printf("%d\n",stack_size(&stack));

	Stack_Destroy(&stack);
}

int main()
{
	teststack1();
	return 0;
}

