#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//输入n科成绩（浮点数表示），统计其中的最高分，最低分以及平均分。
//
//数据范围：1≤n≤100 1 \le n \le 100 \ 1≤n≤100  ， 成绩使用百分制且不可能出现负数
//输入描述：
//两行，
//
//第1行，正整数n（1≤n≤100）
//
//第2行，n科成绩（范围0.0~100.0），用空格分隔。
//输出描述：
//输出一行，三个浮点数，分别表示，最高分，最低分以及平均分（小数点后保留2位），用空格分隔。
int main()
{
    int n = 0;
    scanf("%d", &n);
    float score = 0.0f;
    float max = 0.0f;
    float min = 100.00f;
    float sum = 0.0f;
    int i = 0;
    for (i = 0; i < n; i++)
    {
        scanf("%f", &score);
        if (score > max)
            max = score;
        if (score < min)
            min = score;
        sum += score;
    }
    float ave = sum / n;
    printf("%.2f %.2f %.2f", max, min, ave);
    return 0;
}