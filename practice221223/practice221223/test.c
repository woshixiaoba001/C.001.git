﻿#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//描述
//牛牛最近学会了一些简单的数学运算，例如 ∑i = 1n ∑i = 1n​  请你帮他模拟一下这个运算。 （即 1 + 2 + 3.... + n - 1 + n)
//输入描述：
//输入仅一个正整数 n 
//输出描述：
//请你计算 ∑i = 1n ∑i = 1n​ 
int ret(int x) {
    return (1 + x) * x / 2;
}

int main() {
    int n = 0;
    scanf("%d", &n);
    printf("%d", ret(n));
    return 0;
}