#define _CRT_SECURE_NO_WARNINGS 1
//平衡字符串 中，'L' 和 'R' 字符的数量是相同的。
//
//给你一个平衡字符串 s，请你将它分割成尽可能多的子字符串，并满足：
//
//每个子字符串都是平衡字符串。
//
//返回可以通过分割得到的平衡字符串的 最大数量 。
//
//来源：力扣（LeetCode）
//链接：https ://leetcode.cn/problems/split-a-string-in-balanced-strings
//著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。

#include <iostream>
#include <string>

using namespace std;

class Solution {
public:
    int balancedStringSplit(string s) {
        int count = 0;
        int balance = 0;
        for (int i = 0; i < s.size(); i++)
        {
            if (s[i] == 'L')
            {
                balance--;
                if (balance == 0)
                {
                    count++;
                    continue;
                }
            }

            if (s[i] == 'R')
            {
                balance++;
                if (balance == 0)
                {
                    count++;
                    continue;
                }
            }
        }

        return count;

    }
};