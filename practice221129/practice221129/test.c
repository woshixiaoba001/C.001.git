#define _CRT_SECURE_NO_WARNINGS 1
//给你一个链表的头节点 head ，判断链表中是否有环。
#include <stdbool.h>

 struct ListNode {
     int val;
     struct ListNode *next;
 };
 


bool hasCycle(struct ListNode* head)
{

    struct ListNode* fast = head;
    struct ListNode* slow = head;
    //当fast或者fast->next不是空就继续，说明在环中进行追击
    while (fast && fast->next)
    {
        fast = fast->next->next;
        slow = slow->next;
        if (fast == slow)
            return true;
    }
    //跳出循环说明没有环
    return false;
}