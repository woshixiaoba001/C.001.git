#define _CRT_SECURE_NO_WARNINGS 1
#include "heap_sort.h"

void test1()
{
	int arr[10] = { 23,1,43,64,77,100,2,32,88,54 };

	heap_create(arr, 10);

	int i = 0;
	for (i; i < 10; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");

	heap_sort(arr,10);
	for (i = 0; i < 10; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

int main()
{
	test1();
	return 0;
}