#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int HeapSortDatatype;

//本质就是对随机数组向下调整，形成一个堆的结构
void heap_create(HeapSortDatatype* a, int size);

void heap_sort(HeapSortDatatype* a, int size);

