#define _CRT_SECURE_NO_WARNINGS 1
#include "heap_sort.h"

void swap(HeapSortDatatype* a, HeapSortDatatype* b)
{
	HeapSortDatatype tmp = *a;
	*a = *b;
	*b = tmp;
}

void adjustdown(HeapSortDatatype* a, int size, int parent)
{
	int child = parent * 2 + 1;
	
	while (child<size)
	{
		if (child + 1 < size && a[child + 1] > a[child])
			child++;
		if (a[child] > a[parent])
		{
			swap(&a[parent], &a[child]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
			break;
	}
}

void heap_create(HeapSortDatatype* a, int size)
{
	int child = size - 1;
	int parent = (child - 1) / 2;
	while (parent>=0)
	{
		adjustdown(a, size, parent);
		--parent;
	}
}

void heap_sort(HeapSortDatatype* a, int size)
{
	int last = size - 1;
	while (last > 0)
	{
		swap(&a[0], &a[last]);
		last--;
		adjustdown(a, last+1, 0);

	}
}

