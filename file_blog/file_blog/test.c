#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//int main()
//{
//	//打开文件
//	FILE* pf = fopen("test.txt", "r");
//	if (NULL == pf)
//	{
//		perror("fopen:");
//		return 1;
//	}
//	//使用fgetc读取文件中26个小写字母字符，并打印返回值
//	// 已知文件中有多少个字符并从中读取到内存中
//	//int i = 0;
//	//for (i = 0; i < 26; i++)
//	//{
//	//	printf("%c ",fgetc(pf));
//	//}
//	// 无论文件有多少个字符，将其都读取到内存中
//	int ret = 0;
//	while ( (ret = fgetc(pf)) != EOF)
//	{
//		printf("%c ", ret);
//	}
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}

//int main()
//{
//	//打开文件
//	FILE* pf = fopen("test2.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen:");
//		return 1;
//	}
//	//使用文件
//	char arr[100] = { "**************************************" };
//	char* ret = NULL;
//	if ((ret = fgets(arr, 50, pf)) == NULL)
//	{
//		perror("fgets:");
//		return 1;
//	}
//	printf("%s", ret);
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//struct student
//{
//	char name[20];
//	int age;
//	char sex[10];
//};
//int main()
//{
//	//打开文件
//	FILE* ps = fopen("test3.txt", "w");
//	if (ps == NULL)
//	{
//		perror("fopen:");
//		return 1;
//	}
//	//使用文件
//	fprintf(ps,"%s %d %s", "xiaoba", 22, "nan");
//	//关闭文件
//	fclose(ps);
//	ps == NULL;
//	return 0;
//}

//int main()
//{
//	//打开文件
//	struct student s0 = {0};
//	FILE* ps = fopen("test3.txt", "r");
//	if (ps == NULL)
//	{
//		perror("fopen:");
//		return 1;
//	}
//	//使用文件
//	fscanf(ps, "%s %d %s", s0.name, &(s0.age), s0.sex);
//	printf("%s %d %s", s0.name, s0.age, s0.sex);
//	//关闭文件
//	fclose(ps);
//	ps == NULL;
//	return 0;
//}

//int main()
//{
//	int tmp = fgetc(stdin);
//	fputc(tmp, stdout);
//	return 0;
//}

//struct student
//{
//	char name[20];
//	int age;
//	char sex[10];
//};
//
//int main()
//{
//	//打开文件
//	FILE* pw = fopen("test4.txt", "wb");
//	struct student s1 = { "xiaoba", 22, "nan"};
//	if (pw == NULL)
//	{
//		perror("fopen:");
//		return 1;
//	}
//	//使用文件
//	fwrite(&s1, sizeof(struct student), 1, pw);
//
//	//关闭文件
//	fclose(pw);
//	pw = NULL;
//	return 0;
//}

//struct student
//{
//	char name[20];
//	int age;
//	char sex[10];
//};
//
//int main()
//{
//	//打开文件
//	FILE* pw = fopen("test4.txt", "rb");
//	struct student s1 = { 0 };
//	if (pw == NULL)
//	{
//		perror("fopen:");
//		return 1;
//	}
//	//使用文件
//	fread(&s1, sizeof(struct student), 1, pw);
//	printf("%s %d %s", s1.name, s1.age, s1.sex);
//
//	//关闭文件
//	fclose(pw);
//	pw = NULL;
//	return 0;
//}

//struct student
//{
//	char name[20];
//	int age;
//	char sex[10];
//};
//
//int main()
//{
//	//用sprintf写入字符串
//	char arr[100] = { " " };
//	struct student s0 = { "xiaoba", 99, "nan"};
//	sprintf(arr, "%s %d %s\n", s0.name, s0.age, s0.sex);
//	printf("%s", arr);
//	//用sscanf从字符串读取相应格式放入结构体中
//	struct student x = { 0 };
//	sscanf(arr, "%s %d %s", x.name, &(x.age), x.sex);
//	printf("%s %d %s\n", x.name, x.age, x.sex);
//	return 0;
//}

//int main()
//{
//	//打开文件
//	FILE* pf = fopen("text7.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen:");
//		return 1;
//	}
//	//使用文件
//	fseek(pf, -5, SEEK_END);
//	printf("%c\n",getc(pf));
//	fseek(pf, 2, SEEK_CUR);
//	rewind(pf);
//	int offset = ftell(pf);
//	printf("%d\n", offset);
//	//printf("%c\n", getc(pf));
//	//fseek(pf, 5, SEEK_SET);
//	//printf("%c\n", getc(pf));
//	//int offset = ftell(pf);
//	//printf("%d\n", offset);
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//int main()
//{
//	FILE* pf = fopen("test666.txt", "w");
//	if (pf == NULL)
//	{
//		perror("fopen:");
//		return 1;
//	}
//	int i = 10000;
//	fwrite(&i, sizeof(int), 1, pf);
//	
//	fprintf(pf, "%d", i);
//
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}

int main(void)
{
	int c; // 注意：int，非char，要求处理EOF
	FILE* fp = fopen("test.txt", "r");
	if (!fp) 
	{
		perror("File opening failed");
		return 1;
	}
	//fgetc 当读取失败的时候或者遇到文件结束的时候，都会返回EOF
	while ((c = fgetc(fp)) != EOF) // 标准C I/O读取文件循环
	{
		putchar(c);
	}
	//判断是什么原因结束的
	if (ferror(fp))
		puts("I/O error when reading");
	else if (feof(fp))
		puts("End of file reached successfully");
	fclose(fp);
}