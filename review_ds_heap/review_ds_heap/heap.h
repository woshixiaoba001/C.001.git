#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

typedef int HeapDatatype;

typedef struct STLheap 
{
	HeapDatatype* a;
	int capacity;
	int size;
}HP;

void heap_creat(HP* php);

void heap_initial(HP* php);
void heap_destroy(HP* php);
void heap_print(HP* php);


void heap_push(HP* php, HeapDatatype x);
void heap_pop(HP* php);

HeapDatatype heap_top(HP* php);
int heap_size(HP* php);
bool heap_isempty(HP* php);

void adjustup(HeapDatatype* a, int child);
void adjustdown(HeapDatatype* a, int size, int parent);

