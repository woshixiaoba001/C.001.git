#define _CRT_SECURE_NO_WARNINGS 1
#include "heap.h"

void test1()
{
	HP h1;
	heap_initial(&h1);
	heap_push(&h1, 1);
	heap_push(&h1, 23);
	heap_push(&h1, 32);
	heap_push(&h1, 8);
	heap_push(&h1, 12);
	heap_push(&h1, 45);
	heap_push(&h1, 9);
	heap_push(&h1, 10);
	heap_push(&h1, 100);
	heap_push(&h1, 55);
	heap_push(&h1, 999);
	heap_print(&h1);
	heap_pop(&h1);
	heap_print(&h1);
	heap_pop(&h1);
	heap_print(&h1);	
	heap_pop(&h1);
	heap_print(&h1);

}

int main()
{
	test1();
	return 0;
}