#define _CRT_SECURE_NO_WARNINGS 1
#include "heap.h"

void swap(HeapDatatype* a, HeapDatatype* b)
{
	HeapDatatype tmp = *a;
	*a = *b;
	*b = tmp;
}
void adjustup(HeapDatatype* a, int child)
{
	int parent = (child - 1) / 2;

	while (child>0)
	{
		if (a[child] > a[parent])
		{
			swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
			break;
	}
}
void adjustdown(HeapDatatype* a, int size, int parent)
{
	int child = parent * 2 + 1;

	while (child<size)
	{
		if (child + 1 < size && a[child + 1] > a[child])
			child++;
		if (a[parent] < a[child])
		{
			swap(&a[parent], &a[child]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
			break;
	}
}



void heap_initial(HP* php)
{
	assert(php);
	php->a = NULL;
	php->capacity = php->size = 0;
}

void heap_destroy(HP* php)
{
	assert(php);
	free(php->a);
	php->capacity = php->size = 0;
	php = NULL;
}

void heap_print(HP* php)
{
	assert(php);
	int i = 0;
	for (i; i < php->size; i++)
	{
		printf("%d ", php->a[i]);
	}
	printf("\n");
}

void heap_push(HP* php, HeapDatatype x)
{
	assert(php);
	if (php->size == php->capacity)
	{
		int newcapacity = php->capacity == 0 ? 4 : php->capacity * 2;
		HeapDatatype* tmp = (HeapDatatype*)realloc(php->a, sizeof(HeapDatatype) * newcapacity);
		if (tmp == NULL)
		{
			perror("malloc failed");
			exit(-1);
		}
		php->a = tmp;
	}
	php->size++;
	php->a[php->size - 1] = x;
	adjustup(php->a,php->size-1);
}

void heap_pop(HP* php)
{
	assert(php);
	assert(!heap_isempty(php));
	swap(&(php->a[php->size - 1]), &(php->a[0]));
	php->size--;
	adjustdown(php->a, php->size, 0);
}



HeapDatatype heap_top(HP* php)
{
	assert(php);
	assert(!heap_isempty(php));
	return php->a[0];
}

int heap_size(HP* php)
{
	assert(php);
	return php->size;
}

bool heap_isempty(HP* php)
{
	assert(php);
	return php->size == 0;
}


