#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
//描述
//牛牛从键盘输入一个长度为 n 的数组，问你能否用这个数组组成一个链表，并顺序输出链表每个节点的值。
//输入描述：
//第一行输入一个正整数 n ，表示数组的长度
//输出描述：
//制作一个链表然后输出这个链表的值
typedef struct SL
{
    int val;
    struct SL* next;
}SL;

SL* BuyNode(int X)
{
    SL* newnode = (SL*)malloc(sizeof(SL));
    if (newnode == NULL)
    {
        perror("buynode fail");
        exit(-1);
    }
    newnode->next = NULL;
    newnode->val = X;
    return newnode;
}
int main()
{
    SL* guardhead = (SL*)malloc(sizeof(SL));
    if (guardhead == NULL)
    {
        perror("malloc fail:");
        exit(-1);
    }
    guardhead->val = -1;
    guardhead->next = NULL;
    SL* tail = guardhead;
    int N = 0;
    scanf("%d", &N);
    int i = 0;
    int arr[N];
    while (N--)
    {
        scanf("%d", &arr[i]);
        SL* newnode = BuyNode(arr[i]);
        i++;
        tail->next = newnode;
        tail = tail->next;
    }
    SL* cur = guardhead->next;
    while (cur)
    {
        printf("%d ", cur->val);
        cur = cur->next;
    }
    return 0;
}