#define _CRT_SECURE_NO_WARNINGS 1
#include "contact.h"
#include <stdio.h>

//静态版本通讯录V1
//void initial_contact(struct contact* pi)
//{
//	memset(pi->PI, 0, sizeof(struct peopleinfo) * MAX);
//	pi->sz = 0;
//}


//导入通讯录数据
int check_capacity(struct contact* pi);

void import_data(struct contact* pi)
{
	//打开文件
	FILE* PF = fopen("contact_data.txt", "rb");
	if (PF == NULL)
	{
		perror("import_data:fopen");
		return;
	}
	//使用文件
	struct peopleinfo TP = { 0 };
	while(1 == fread(&TP,sizeof(struct peopleinfo),1,PF))
	{
		check_capacity(pi);

		pi->PI[pi->sz] = TP;
		pi->sz++;
	}
	//关闭文件
	fclose(PF);
	PF = NULL;
}
//动态版本通讯录V2
void initial_contact(struct contact* pi)
{
	pi->PI = (struct peopleinfo*)malloc(INITIAL_SIZE * sizeof(struct peopleinfo));
	pi->capacity = INITIAL_SIZE;
	pi->sz = 0;
	import_data(pi);
}

//静态通讯录版本V1
//void add(struct contact* pi)
//{
//	if (pi->sz == MAX)
//	{
//		printf("通讯录已满，无法添加\n");
//	}
//	else 
//	{	
//		printf("请输入姓名\n");
//		scanf("%s", pi->PI[pi->sz].name);
//		printf("请输入年龄\n");
//		scanf("%d", &(pi->PI[pi->sz].age));
//		printf("请输入性别\n");
//		scanf("%s", pi->PI[pi->sz].sex);
//		printf("请输入电话\n");
//		scanf("%s", pi->PI[pi->sz].tel);
//		printf("请输入地址\n");
//		scanf("%s", pi->PI[pi->sz].adress);
//		pi->sz++;
//		printf("成功增加联系人\n");
//		printf("%d\n", pi->sz);
//	}
//}

//动态通讯录版本V2
int check_capacity(struct contact* pi)
{
	if (pi->sz == pi->capacity)
	{
		struct peopleinfo* tmp = (struct peopleinfo*)realloc(pi->PI, (pi->sz + INC_SIZE) * sizeof(struct peopleinfo));
		if (tmp == NULL)
			return 0;
		pi->PI = tmp;
		pi->capacity = INC_SIZE + pi->sz;
		printf("增容成功\n");
		return 1;
	}
	return 1;
}

void add(struct contact* pi)
{
	int ret = check_capacity(pi);
	if (0 == ret)
	{
		perror("check_capacity:");
		return;
	}
	printf("请输入姓名\n");
	scanf("%s", pi->PI[pi->sz].name);
	printf("请输入年龄\n");
	scanf("%d", &(pi->PI[pi->sz].age));
	printf("请输入性别\n");
	scanf("%s", pi->PI[pi->sz].sex);
	printf("请输入电话\n");
	scanf("%s", pi->PI[pi->sz].tel);
	printf("请输入地址\n");
	scanf("%s", pi->PI[pi->sz].adress);
	pi->sz++;
	printf("成功增加联系人\n");
	printf("%d\n", pi->sz);

}

void show(const struct contact* pi)
{
	printf("%-20s\t%-10s\t%-10s\t%-11s\t%-20s\n", "姓名", "年龄", "性别", "电话", "住址");
	int i = 0;
	for (i = 0; i < pi->sz; i++)
	{
		printf("%-20s\t%-10d\t%-10s\t%-11s\t%-20s\n", pi->PI[i].name, pi->PI[i].age, pi->PI[i].sex, pi->PI[i].tel, pi->PI[i].adress);
	}
}
//查找指定联系人
static int findbyname(struct contact* pi, char name[])
{
	int i = 0;
	for (i = 0; i < pi->sz; i++)
	{
		if (strcmp(name, pi->PI[i].name) == 0)
			return i;
	}
	return -1;
}

//删除指定联系人
void del(struct contact* pi)
{
	char name[NAME];
	printf("请输入要删除的联系人\n");
	scanf("%s", name);
	int ret = findbyname(pi, name);
	if (-1 == ret)
	{
		printf("无此联系人\n");
	}
	else
	{
		int j = 0;
		for (j = ret; j < pi->sz - 1; j++)
		{
			pi->PI[j] = pi->PI[j + 1];
		}
		pi->sz--;
		printf("删除成功\n");
	}
}
//修改联系人
void modify(struct contact* pi)
{
	char name[MAX];
	printf("请输入需要修改的联系人名字\n");
	scanf("%s", name);
	int ret = findbyname(pi, name);
	if (-1 == ret)
	{
		printf("无此联系人\n");
	}
	else
	{
		printf("请输入修改的姓名\n");
		scanf("%s", pi->PI[ret].name);
		printf("请输入修改的年龄\n");
		scanf("%d", &(pi->PI[ret].age));
		printf("请输入修改的性别\n");
		scanf("%s", pi->PI[ret].sex);
		printf("请输入修改的电话\n");
		scanf("%s", pi->PI[ret].tel);
		printf("请输入修改的地址\n");
		scanf("%s", pi->PI[ret].adress);
		printf("成功修改联系人\n");
	}
}
//查找通讯录
void search(const struct contact* pi)
{
	char name[MAX];
	printf("请输入需要查找的联系人名字\n");
	scanf("%s", name);
	int ret = findbyname(pi, name);
	if (-1 == ret)
	{
		printf("无此联系人\n");
	}
	else
	{
		printf("%-20s\t%-10s\t%-10s\t%-11s\t%-20s\n", "姓名", "年龄", "性别", "电话", "住址");
		printf("%-20s\t%-10d\t%-10s\t%-11s\t%-20s\n", pi->PI[ret].name, pi->PI[ret].age, pi->PI[ret].sex, pi->PI[ret].tel, pi->PI[ret].adress);
	}
}

//排序通讯录byname
int cmpbyname(void* e1, void* e2)
{
	return strcmp(((struct peopleinfo*)e1)->name, ((struct peopleinfo*)e2)->name);
}
void sort(struct contact* pi)
{
	qsort(pi->PI, pi->sz, sizeof(struct peopleinfo), cmpbyname);
}

//动态通讯录V2释放动态内存空间函数实现
void destory_contact(struct contact* pi)
{
	free(pi->PI);
	pi->PI = NULL;
	pi->sz = 0;
	pi->capacity = 0;
	printf("成功销毁通讯录\n");
}

//动态通讯录V3关闭程序保存通讯录信息的函数实现
void save_contact(struct contact* pi)
{
	//打开文件
	FILE* PF = fopen("contact_data.txt", "wb");
	if (PF == NULL)
	{
		perror("save_contact:fopen");
		return;
	}
	//写入通讯录信息
	int i = 0;
	for (i = 0; i < pi->sz; i++)
	{
		fwrite(pi->PI+i,sizeof(struct peopleinfo),1,PF);
	}
	//关闭文件
	fclose(PF);
	PF = NULL;
}


