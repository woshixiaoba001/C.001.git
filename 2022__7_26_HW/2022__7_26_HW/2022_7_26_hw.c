#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//将数组A中的内容和数组B中的内容进行交换。（数组一样大）

//void swap_arr(int* p1, int* p2,int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		int tmp = 0;
//		tmp = *(p1 + i);
//		*(p1 + i) = *(p2 + i);
//		*(p2 + i) = tmp;
//	}
//}
//
//int main()
//{
//	int sta1[5] = {1,2,3,4,5};
//	int sta2[5] = { 6,7,8,9,10 };
//	int sz = sizeof(sta1) / sizeof(sta1[0]);
//	swap_arr(sta1, sta2,sz);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", sta1[i]);
//	}
//	printf("\n");
//
//	for (i = 0; i <sz; i++)
//	{
//		printf("%d ", sta2[i]);
//	}
//	printf("\n");
//	return 0;
//}
//创建一个整形数组，完成对数组的操作
//
//实现函数init() 初始化数组为全0
//实现print()  打印数组的每个元素
//实现reverse()  函数完成数组元素的逆置。

//void init(int* arr, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		*(arr + i) = 0;
//	}
//}
//
//void print(int* arr, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(arr + i));
//	}
//}
//
//void reverse(int* arr, int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//	int i = 0;
//	for (i = 0; i <= sz / 2; i++)
//	{
//		int tmp = 0;
//		tmp = *(arr + i);
//		*(arr + i) = *(arr + sz - 1 - i);
//		*(arr + sz - 1 - i) = tmp;
//	}
//}
//
//
//int main()
//{
//	int sta[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(sta) / sizeof(sta[0]);
//	reverse(sta, sz);
//	init(sta, sz);
//	print(sta, sz);
//	return 0;
//}

//实现一个对整形数组的冒泡排序
void bubble(int* arr, int sz)
{
	int i = 0;
	for (i = 0; i < sz-1; i++)
	{
		int j = 0;
		for (j=0; j<sz-1-i; j++)
		{
			int tmp = 0;
			if (arr[j] > arr[j + 1])
			{
				tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
			}
		}
	}
}

int main()
{
	int sta[10] = { 5,8,6,9,7,1,4,11,2,3 };
	int sz = sizeof(sta) / sizeof(sta[0]);

	bubble(sta, sz);
	
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", sta[i]);
	}
	return 0;
}