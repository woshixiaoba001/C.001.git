#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

typedef int SLDatatype;

typedef struct SingleList
{
	SLDatatype* a;
	int size;
	int capacity;
}SL;

void SLinitial(SL* s);
void SLdestroy(SL* s);
void SLprintf(SL* s);

void SLpushback(SL* s, SLDatatype x);
void SLpopback(SL* s);

void SLpushfront(SL* s, SLDatatype x);
void SLpopfront(SL* s);

void SLpospush(SL* s, SLDatatype x, int pos);
void SLpospop(SL* s, int pos);

int SLfindpos(SL* s, SLDatatype x);


