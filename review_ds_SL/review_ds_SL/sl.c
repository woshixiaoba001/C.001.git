#include "sl.h"
void checkcapacity(SL* s)
{
	int newcapacity = 0;
	if (s->capacity == s->size)
	{
		newcapacity = (s->capacity == 0 ? 4 : s->capacity * 2);
		SLDatatype* tmp = (SLDatatype*)realloc(s->a, sizeof(SLDatatype) * newcapacity);
		if (tmp == NULL)
		{
			perror("realloc failed");
			exit(-1);
		}
		s->a = tmp;
		s->capacity = newcapacity;
	}
}

void SLinitial(SL* s) 
{
	assert(s);
	s->a = NULL;
	s->capacity = s->size = 0;
}
void SLdestroy(SL* s)
{
	assert(s);
	free(s->a);
	s->capacity = s->size = 0;
}
void SLprintf(SL* s)
{
	int i = 0;
	for (i = 0; i < s->size; i++)
	{
		printf("%d ", s->a[i]);
	}
	printf("\n");
}


void SLpushback(SL* s, SLDatatype x)
{
	assert(s);
	checkcapacity(s);
	s->a[s->size] = x;
	s->size++;
}
void SLpopback(SL* s)
{
	assert(s);
	assert(s->size > 0);
	s->size--;
}

void SLpushfront(SL* s, SLDatatype x)
{
	assert(s);
	checkcapacity(s);
	int end = s->size - 1;
	while (end >= 0)
	{
		s->a[end + 1] = s->a[end];
		end--;
	}
	s->a[0] = x;
	s->size++;
}
void SLpopfront(SL* s)
{
	assert(s);
	assert(s->size > 0);
	int begin = 1;
	while (begin < s->size)
	{
		s->a[begin - 1] = s->a[begin];
		begin++;
	}
	s->size--;
}

void SLpospush(SL* s, SLDatatype x, int pos)
{
	assert(s);
	checkcapacity(s);
	int end = s->size - 1;
	while (end >= pos)
	{
		s->a[end + 1] = s->a[end];
		end--;
	}
	s->a[pos] = x;
	s->size++;
}
void SLpospop(SL* s, int pos)
{
	assert(s);
	assert(s->size > 0);
	assert(pos > 0 && pos <= s->size);
	int begin = pos;
	while (begin < s->size)
	{
		s->a[begin - 1] = s->a[begin];
		begin++;
	}
	s->size--;
}

int SLfindpos(SL* s, SLDatatype x)
{
	assert(s);
	int i = 0;
	for (i; i < s->size; i++)
	{
		if (s->a[i] == x)
			return i+1;
	}
	if (i == s->size)
		return -1;
}






