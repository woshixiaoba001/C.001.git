#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//写一个代码打印1-100之间所有3的倍数的数字
//int main()
//{
//	int n = 0;
//	int mo = 0;
//	int count = 0;
//	for (n = 1; 100 >= n; n++)
//	{
//		if ((n % 3) == 0)
//		{
//			count++;
//			if ((count % 10) == 0 && count > 9)
//				printf("%d\n", n);
//			else
//				printf("%d  ", n);
//		}
//	}
//	return 0;
//}

//写代码将三个整数数按从大到小输出。
//
//例如：
//
//输入：2 3 1
//
//输出：3 2 1

//int main()
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	int max = 0;
//	scanf("%d %d %d",&a,&b,&c);
//	max = c;
//	if (b > max)
//		max = b;
//	else if(a > max)
//		max = a;
//	printf("%d ", max);
//	
//	if (a == max)
//		if(b>c)
//		    printf("%d %d", b, c);
//		else
//			printf("%d %d", c, b);
//	if( b == max)
//		if(a>c)
//			printf("%d %d", a, c);
//		else
//			printf("%d %d", c, a);
//	if(c == max)
//		if(a>b)
//			printf("%d %d", a, b);
//		else
//			printf("%d %d", b, a);
//	return 0;
//}

//写一个代码：打印100~200之间的素数
//int main()
//{
//	int n = 0;
//	int i = 0;
//	
//	for (n = 100; n <= 200; n ++)
//	{
//		if (n % 2 != 0)
//		{
//			int count = 0;
//			for (i = 1; i < n; i++)
//			{
//				if ((n % (n - i)) == 0)
//					count++;
//			}
//			if (1 == count)
//				printf("%d\n", n);
//		}
//	}
//
//	return 0;
//}

//打印1000年到2000年之间的闰年
//int main()
//{
//	int year = 0;
//	int run = 0;
//	int count = 0;
//	for (year = 1000; year <= 2000; year++)
//	{
//		if (0 == year % 4 && year % 100 != 0 || 0 == year % 400)
//		{
//			count++;
//			printf("%d  ", year);
//			if(0 == count%10)
//				printf("%d\n", year);
//		}
//	}
//
//	printf("%d\n", year);
//	return 0;
//}

//给定两个数，求这两个数的最大公约数
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int ret = 0;
//	scanf("%d %d",&a,&b);
//
//	if (a > b)
//	{
//		int i = 0;
//		for (i = 0; i <= b-1; i++)
//		{
//			if ((0 == a % (b - i)) && (0 == b %(b - i)))
//			{
//				ret = b - i;
//				printf("%d\n", ret);
//					break;
//			}
//		}
//	}
//	else
//	{
//		int i = 0;
//		for (i = 0; i <= a - 1; i++) 
//		{
//			if ((0 == b % (a - i)) && (0 == a % (a - i)))
//			{
//				ret = a - i;
//				printf("%d\n", ret);
//				break;
//			}
//		}
//	}
//
//	return 0;
//}

//在屏幕上输出9 * 9乘法口诀表
//int main()
//{
//	int row = 0;
//	int col = 0;
//	int sum = 0;
//	int count = 0;
//	
//	for(row = 1; row <= 9; row++)
//	{ 
//		for (col = 1; col <= row; col++)
//		{
//			count++;
//			sum = col * row;
//			printf("%d*%d=%d  ", col, row, sum);	
//	    }
//		printf("\n");
//	}
//	return 0;
//}

//求10 个整数中最大值
int main()
{
	int arr[10] = { 100,59,23,45,65,87,23,45,12,32 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int left = 0;
	int right = sz-1;
	int max = 0;
	
	
	while (left<=right)
	{
		if (arr[left] > arr[right])
		{
			max = arr[left];
			right--;
		}
		else
		{
			max = arr[right];
			left++;
		}
	}
	printf("max is %d\n", max);
	return 0;
}