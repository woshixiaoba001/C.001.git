﻿#define _CRT_SECURE_NO_WARNINGS 1
#include <stdlib.h>

//给你两个单链表的头节点 headA 和 headB ，请你找出并返回两个单链表相交的起始节点。如果两个链表不存在相交节点，返回 null 。
struct ListNode
{
    int val;
    struct ListNode* next;
};

struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB)
{
    struct ListNode* guardA, * guardB, * curA, * curB, * intersec = NULL;
    guardA = (struct ListNode*)malloc(sizeof(struct ListNode));
    if (guardA == NULL)
        exit(-1);
    guardB = (struct ListNode*)malloc(sizeof(struct ListNode));
    if (guardB == NULL)
        exit(-1);
    guardB->next = curB = headB;
    guardA->next = curA = headA;

    while (curA)
    {
        struct ListNode* tmpB = curB;
        while (tmpB)
        {
            if (curA == tmpB)
                return tmpB;
            tmpB = tmpB->next;
        }
        curA = curA->next;
    }

    free(guardA);
    guardA = NULL;
    free(guardB);
    guardB = NULL;
    return NULL;
}