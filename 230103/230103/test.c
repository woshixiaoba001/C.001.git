#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//输出九九乘法表，输出格式见样例。


int main() {
    for (int i = 1; i <= 9; i++) {
        for (int j = 1; j <= i; j++) {
            printf("%d*%d=%2d ", j, i, i * j);
        }
        printf("\n");
    }
    return 0;
}