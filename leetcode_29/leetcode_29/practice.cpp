﻿#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <algorithm>
using namespace std;

//给你两个整数，被除数 dividend 和除数 divisor。将两数相除，要求 不使用 乘法、除法和取余运算。
//
//整数除法应该向零截断，也就是截去（truncate）其小数部分。例如，8.345 将被截断为 8 ， - 2.7335 将被截断至 - 2 。
//
//返回被除数 dividend 除以除数 divisor 得到的 商 。
//
//注意：假设我们的环境只能存储 32 位 有符号整数，其数值范围是[−231, 231 − 1] 。本题中，如果商 严格大于 231 − 1 ，则返回 231 − 1 ；如果商 严格小于 - 231 ，则返回 - 231 。
//
//来源：力扣（LeetCode）
//链接：https ://leetcode.cn/problems/divide-two-integers
//著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。



class Solution {
public:
    int sub(int dividend, int divisor)
    {
        int pos = 0;
        if (dividend < divisor)
            return count;
        int tmp = divisor;
        while (dividend > tmp)
        {
            tmp = tmp << 1;
            pos++;
        }
        pos--;
        dividend = dividend - divisor * pow(2, pos);
        count += pow(2, pos);
        cout << dividend << endl;
        return sub(dividend, divisor);
    }



    int divide(int dividend, int divisor) {
        int ret = 0;
        int flag = 1;

        if (dividend < 0 && divisor < 0)
            flag = 1;
        else if (dividend < 0 || divisor < 0)
            flag = -1;
        dividend = abs(dividend);
        divisor = abs(divisor);

        if (dividend == 0)
            return 0;
        return sub(dividend, divisor) * flag;
    }

    static int count;

};

int Solution::count = 0;