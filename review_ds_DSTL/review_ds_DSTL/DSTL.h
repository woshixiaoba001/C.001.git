#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

typedef int DSTLdatatype;

typedef struct DSTLnode 
{
	struct DSTLnode* next;
	struct DSTLnode* prev;
	DSTLdatatype val;
}DSTL;

DSTL* DSTLinitial();
DSTL* buynode(DSTLdatatype x);
void print(DSTL* phead);

void DSTLinsert(DSTL* pos, DSTLdatatype x);
void DSTLerase(DSTL* pos);

void DSTLpushback(DSTL* phead, DSTLdatatype x);
void DSTLpopback(DSTL* phead);

void DSTLpushfront(DSTL* phead, DSTLdatatype x);
void DSTLpopfront(DSTL* phead);

void DSTLdestroy(DSTL* phead);
DSTL* find(DSTL* phead, DSTLdatatype x);
bool isempty(DSTL* phead);
size_t size(DSTL* phead);