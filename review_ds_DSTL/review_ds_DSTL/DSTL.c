#include "DSTL.h"

DSTL* DSTLinitial()
{
	DSTL* phead = buynode(-1);
	phead->next = phead;
	phead->prev = phead;
	return phead;
}

DSTL* buynode(DSTLdatatype x)
{
	DSTL* newnode = (DSTL*)malloc(sizeof(DSTL));
	if (newnode == NULL)
	{
		perror("malloc failed");
		exit(-1);
	}
	newnode->next = NULL;
	newnode->prev = NULL;
	newnode->val = x;
	return newnode;
}


void print(DSTL* phead)
{
	assert(phead);
	DSTL* cur = phead->next;
	while (cur != phead)
	{
		printf("%d ", cur->val);
		cur = cur->next;
	}
	printf("\n");
}

void DSTLinsert(DSTL* pos, DSTLdatatype x)
{
	assert(pos);
	DSTL* newnode = buynode(x);
	DSTL* prev = pos->prev;
	prev->next = newnode;
	newnode->prev = prev;

	newnode->next = pos;
	pos->prev = newnode;
}

void DSTLerase(DSTL* pos)
{
	assert(pos);
	assert(pos->next != pos);
	DSTL* prev = pos->prev;
	DSTL* next = pos->next;

	free(pos);
	prev->next = next;
	next->prev = prev;
}

void DSTLpushback(DSTL* phead, DSTLdatatype x)
{
	assert(phead);
	DSTLinsert(phead, x);
}
void DSTLpopback(DSTL* phead)
{
	assert(phead);
	DSTLerase(phead->prev);
}

void DSTLpushfront(DSTL* phead, DSTLdatatype x)
{
	assert(phead);
	DSTLinsert(phead->next, x);
}
void DSTLpopfront(DSTL* phead)
{
	assert(phead);
	DSTLerase(phead->next);
}

void DSTLdestroy(DSTL* phead)
{
	assert(phead);
	DSTL* cur = phead->next;
	while (cur != phead)
	{
		DSTL* next = cur->next;
		free(cur);
		cur = next;
	}
	free(phead);
}

DSTL* find(DSTL* phead, DSTLdatatype x)
{
	assert(phead);
	DSTL* cur = phead->next;
	while (cur!= phead)
	{
		if (cur->val == x)
			return cur;
		cur = cur->next;
	}
	return NULL;
}
bool isempty(DSTL* phead)
{
	assert(phead);
	return phead == phead->next;
}
size_t size(DSTL* phead)
{
	assert(phead);
	DSTL* cur = phead->next;
	size_t i = 0;
	while (cur!=phead)
	{
		i++;
		cur = cur->next;
	}
	return i;
}