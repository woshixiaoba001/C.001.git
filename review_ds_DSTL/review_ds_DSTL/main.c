#define _CRT_SECURE_NO_WARNINGS 1
#include "DSTL.h"

void test1()
{
	DSTL* s1 = NULL;
	s1 = DSTLinitial();
	//DSTLinsert(s1, 1);
	//DSTLinsert(s1, 2);
	//DSTLinsert(s1, 3);
	//DSTLinsert(s1, 4);
	DSTLpushback(s1, 1);
	print(s1);
	DSTLpushback(s1, 2);
	print(s1);

	DSTLpushback(s1, 3);
	print(s1);

	DSTLpushback(s1, 4);
	print(s1);

	DSTLpushback(s1, 5);
	print(s1);
	DSTLpopback(s1);
	print(s1);
	DSTLpopback(s1);
	print(s1);	
	DSTLpopback(s1);
	print(s1);
	DSTLpopback(s1);
	print(s1);
	DSTLpopback(s1);
	print(s1);

}

void test2()
{
	DSTL* s2 = NULL;
	s2 = DSTLinitial();
	DSTLpushfront(s2, 1);
	print(s2);
	DSTLpushfront(s2, 2);
	print(s2);
	DSTLpushfront(s2, 3);
	print(s2);
	DSTLpushfront(s2, 4);
	//print(s2);
	//DSTLpopfront(s2);
	//print(s2);
	//DSTLpopfront(s2);
	//print(s2);
	//DSTLpopfront(s2);
	//print(s2);
	//DSTLpopfront(s2);
	//print(s2);
	DSTL* pos = find(s2, 3);
	DSTLinsert(pos, 100);
	print(s2);
	printf("%zd", size(s2));
}

int main()
{
	//test1();
	test2();
	return 0;
}