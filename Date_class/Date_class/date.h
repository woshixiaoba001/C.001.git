#pragma once
#include <iostream>

using namespace std;

class Date
{
public:
	Date(int year = 2023, int month = 3, int day = 15)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	Date(const Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}
	~Date()
	{}

	Date& operator=(const Date& d) 
	{
		if (this != &d)
		{
			 _year = d._year;
			 _month = d._month;
			 _day = d._day;
		}
		return *this;
	}

	void print();
	int getMonthDay(int year, int month);
	Date& operator+=(int x);
	Date operator+(int x);

	Date operator-(int day);
	Date& operator-=(int day);

	Date& operator++();
	Date operator++(int);

	Date& operator--();
	Date operator--(int);

	bool operator>(const Date& d);
	bool operator==(const Date& d);
	bool operator >= (const Date& d);
	
	bool operator < (const Date& d);
	bool operator <= (const Date& d);
	bool operator != (const Date& d);

	int operator-(const Date& d);

private:
	int _year;
	int _month;
	int _day;
};
