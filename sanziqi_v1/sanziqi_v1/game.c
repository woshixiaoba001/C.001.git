#define _CRT_SECURE_NO_WARNINGS 1
#include "game.h"

//打印棋盘的函数定义
void display_board(char arr[ROW][COL], int row, int col)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			if (col-1 > j)
			{
				printf(" %c |", arr[i][j]);
			}
			else
			{
				printf(" %c ", arr[i][j]);
			}
		}
		printf("\n");
		if (i < row - 1)
		{
			for (j = 0; j < col; j++)
			{
				if (col - 1 > j)
				{
					printf("---|");
				}
				else
				{
					printf("---");
				}
			}
			printf("\n");
		}
	}
}

//初始化棋盘
void initial_board(char arr[ROW][COL], int row, int col)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			arr[i][j] = ' ';
		}
	}
}

//玩家下棋
void player_move(char arr[ROW][COL], int row, int col)
{
	printf("请输入坐标\n");
	int a = 0;
	int b = 0;
	while (1)
	{
		scanf("%d %d", &a, &b);
		if (1 <= a && a <= row && 1 <= b && b <= col)
		{
			if (arr[a - 1][b - 1] == ' ')
			{
				arr[a - 1][b - 1] = '*';
				break;
			}
			else
			{
				printf("已被占用，请重新输入\n");
			}
		}
		else
			printf("坐标超出范围，请重新输入坐标\n");
	}	
}
//电脑下棋
void computer_move(char arr[ROW][COL], int row, int col)
{
	int a = 0;
	int b = 0;

	while (1)
	{
		a = rand() % row;
		b = rand() % col;
		if (arr[a][b] == ' ')
		{
			arr[a][b] = '#';
			break;
		}
	}
	printf("电脑已走\n");
}
//判断输赢
char is_win(char arr[ROW][COL], int row, int col)
{
	int i = 0;
	int j = 0;

	//row is same
	for (i = 0; i < row; i++)
	{
		if ((arr[i][0] == arr[i][1]) && (arr[i][1] == arr[i][2]) && arr[i][0] != ' ')
		{
			return arr[i][1];
		}
	}
	//col is same
	for (j = 0; j < col; j++)
	{
		if ((arr[0][j] == arr[1][j]) && (arr[1][j] == arr[2][j]) && arr[0][j] != ' ')
		{
			return arr[1][j];
		}
	}
	//diagonal is same
	if ((arr[0][0] == arr[1][1]) && (arr[1][1] == arr[2][2]) && arr[0][0] != ' ')
	{
		return arr[1][1];
	}
	if ((arr[0][2] == arr[1][1]) && (arr[1][1] == arr[2][0]) && arr[2][0] != ' ')
	{
		return arr[0][2];
	}
	//is tie game?
	int flag = 1;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			if (arr[i][j] == ' ')
			{
				flag = 0;
				break;
			}		
		}
	}
	if (flag)
		return 'T';
	//continue
	return 'C';
}
