#define _CRT_SECURE_NO_WARNINGS 1
#include "game.h"

void game()
{
	char arr[ROW][COL] = {0};
	initial_board(arr, ROW, COL);
	display_board(arr, ROW, COL);

	char ret = 0;
	while (1)
	{
		player_move(arr, ROW, COL);
		display_board(arr, ROW, COL);
		ret = is_win(arr, ROW, COL);
		if (ret != 'C')
			break;

		computer_move(arr, ROW, COL);
		display_board(arr, ROW, COL);
		ret = is_win(arr, ROW, COL);
		if (ret != 'C')
			break;
	}
	if (ret == '*')
		printf("玩家胜利,游戏结束\n");
	else if (ret == '#')
		printf("电脑胜利，游戏结束\n");
	else if(ret == 'T')
		printf("平局，游戏结束\n");
	display_board(arr, ROW, COL);

}

void menu()
{
	printf("**********************\n");
	printf("******  1.start  *****\n");
	printf("******  0.end    *****\n");
	printf("**********************\n");
}

int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请输入选择\n");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("错误输入，请重新输入\n");
			break;
		}
	} while (input);

	return 0;
}
