#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>

using namespace std;
//int main() {
//
//    char c;
//    int i;
//    long l;
//    double d;
//    cout << sizeof(char) << endl << sizeof(int) << endl << sizeof(long) << endl << sizeof(double) << endl;
//
//
//    return 0;
//}

void bubble(vector<int>& a)
{
	for (int i = 0; i < a.size(); i++)
	{
		for (int j = i+1; j < a.size(); j++)
		{
			if (a[i] > a[j])
				swap(a[i], a[j]);
		}
	}
}

int main()
{
	srand(time(nullptr));
	vector<int> a;
	a.reserve(100);
	for (int i = 0;i<100;i++)
	{
		a.push_back(rand()%100+1);
	}
	
	bubble(a);
	for (auto e : a)
	{
		cout << e << ' ' ;
	}
	return 0;
}