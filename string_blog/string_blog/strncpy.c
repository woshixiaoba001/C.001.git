#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <assert.h>

char* my_strncpy(char* des, const char* src, size_t n)
{	
	assert(des && src);
	char* tmp = des;
	int count = n;
	//一定要count的判断放前面，否则会先赋值再判断count,出现多拷贝一个字符情况
	while (count && (*tmp++ = *src++) != 0)
	{
		count--;
	}
	if (count != 0)
	{
		//一定要前置--，否则会多赋值一个'\0'，因为进入这里的情况是count不为零，前面没有对其count--；
		while (--count)
		{
			*tmp++ = 0;
		}
	}
	return des;
}
int main()
{
	char arr[10] = { "xxxxxxxxx" };
	char* p = "abc";
	int n = 2; /*scanf("%d", &n);*/
	printf("%s", my_strncpy(arr, p,n));
	return 0;
}