#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
//描述
//牛牛输入了一个长度为 n 的数组，他把这个数组转换成链表并在第 i 个节点的后面添加一个值为 i 的新节点
//输入描述：
//第一行输入两个正整数分别是 n 和 i ，表示数组的长度、需要添加节点的位置和节点的值
//第二行输入 n 个正整数表示数组中每个元素的值。
//
//输出描述：
//把数组转换成链表并在第 i 个节点后的添加一个新节点值，新节点的值是 i。
typedef struct SLnode {
    int val;
    struct SLnode* next;
}SL;

SL* BuyNode(int x) {
    SL* newnode = (SL*)malloc(sizeof(SL));
    if (newnode == NULL) {
        perror("malloc fail:");
        exit(-1);
    }
    newnode->val = x;
    newnode->next = NULL;
    return newnode;
}
int main() {
    int n = 0;
    scanf("%d", &n);
    int insert = 0;
    scanf("%d", &insert);
    int arr[n];
    for (int j = 0; j < n; j++) {
        scanf("%d", &arr[j]);
    }
    SL* guardhead = (SL*)malloc(sizeof(SL));
    if (guardhead == NULL) {
        perror("malloc fail:");
        exit(-1);
    }
    guardhead->next = NULL;
    guardhead->val = -1;
    SL* tail = guardhead;
    for (int i = 0; i < n; i++) {
        tail->next = BuyNode(arr[i]);
        tail = tail->next;
    }

    if (insert > n) {
        tail->next = BuyNode(insert);
        tail = tail->next;
    }
    else if (insert < 1) {
        SL* next = guardhead->next;
        guardhead->next = BuyNode(insert);
        guardhead->next->next = next;
    }
    else {
        SL* cur = guardhead;
        int count = insert;
        while (count--) {
            cur = cur->next;
        }
        SL* next = cur->next;
        cur->next = BuyNode(insert);
        cur->next->next = next;
    }
    SL* cur = guardhead->next;
    while (cur) {
        printf("%d ", cur->val);
        SL* del = cur;
        cur = cur->next;
        free(del);
    }
    free(guardhead);
    guardhead = NULL;
    return 0;
}