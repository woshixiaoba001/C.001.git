#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>

//输入一个链表，输出该链表中倒数第k个结点。
struct ListNode 
{
    int val;
    struct ListNode* next;
    
};
struct ListNode* FindKthToTail(struct ListNode* pListHead, int k)
{
    // write code here

    struct ListNode* fast = pListHead;
    struct ListNode* slow = pListHead;

    while (k--)
    {
        if (fast == NULL)
            return NULL;
        fast = fast->next;
    }

    while (fast != NULL)
    {
        slow = slow->next;
        fast = fast->next;
    }
    return slow;

}