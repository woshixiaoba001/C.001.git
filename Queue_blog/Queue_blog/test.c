#define _CRT_SECURE_NO_WARNINGS 1
#include "Queue.h"
void Queuetest1()
{
	Q q1;
	Queue_initial(&q1);
	Queue_push(&q1, 1);
	Queue_push(&q1, 2);
	Queue_push(&q1, 3);
	Queue_push(&q1, 4);
	printf("%d\n", is_queue_empty(&q1));
	printf("%d\n", queue_size(&q1));
	printf("%d\n", Queue_back(&q1));
	while (!is_queue_empty(&q1))
	{
		printf("%d ", Queue_front(&q1));
		Queue_pop(&q1);
	}
	printf("\n");
	printf("%d\n",is_queue_empty(&q1));
	printf("%d\n", queue_size(&q1));


	Queue_Destroy(&q1);
}

int main()
{
	Queuetest1();
	return 0;
}