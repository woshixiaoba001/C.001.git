#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

typedef int QDataType;

typedef struct QueueNode 
{	
	QDataType val;
	struct QueueNode* next;
}QNode;

typedef struct Queue
{
	QNode* head;
	QNode* tail;
	int size;
}Q;

//队列初始化
void Queue_initial(Q* pq);
//队列销毁
void Queue_Destroy(Q* pq);

//数据入队
void Queue_push(Q* pq, QDataType X);
//数据出队
void Queue_pop(Q* pq);

//队头数据
QDataType Queue_front(Q* pq);
//队尾数据
QDataType Queue_back(Q* pq);

//队列是否为空
bool is_queue_empty(Q* pq);
//队列元素个数
int queue_size(Q* pq);