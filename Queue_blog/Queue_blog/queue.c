#define _CRT_SECURE_NO_WARNINGS 1
#include "Queue.h"
//队列初始化
void Queue_initial(Q* pq)
{
	assert(pq);
	pq->head = NULL;
	pq->tail = NULL;
	pq->size = 0;
}
//队列销毁
void Queue_Destroy(Q* pq)
{
	assert(pq);
	while (pq->head)
	{
		pq->head->val = 0;
		QNode* next = pq->head->next;
		free(pq->head);
		pq->head = next;
	}
	pq->head = NULL;
	pq->tail = NULL;
	pq->size = 0;
}
//数据入队
void Queue_push(Q* pq, QDataType X)
{
	assert(pq);
	QNode* newnode = (QNode*)malloc(sizeof(QNode));
	if (newnode == NULL)
	{
		perror("malloc fail:");
		exit(-1);
	}
	newnode->val = X;
	newnode->next = NULL;
	if (pq->head == NULL)
	{
		pq->head = pq->tail = newnode;
	}
	else
	{
		pq->tail->next = newnode;
		pq->tail = pq->tail->next;
	}
	pq->size++;
}
//数据出队
void Queue_pop(Q* pq)
{
	assert(pq);
	assert(!is_queue_empty(pq));
	QNode* next = pq->head->next;
	free(pq->head);
	pq->head = next;
	if (pq->head == NULL)
		pq->tail = NULL;
	pq->size--;
}

//队头数据
QDataType Queue_front(Q* pq)
{
	assert(pq);
	assert(!is_queue_empty(pq));
	return pq->head->val;
}
//队尾数据
QDataType Queue_back(Q* pq)
{
	assert(pq);
	assert(!is_queue_empty(pq));
	return pq->tail->val;
}

//队列是否为空
bool is_queue_empty(Q* pq)
{
	assert(pq);
	return pq->head == NULL && pq->tail == NULL;
}
//队列元素个数
int queue_size(Q* pq)
{
	assert(pq);
	return pq->size;
}