#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>

using namespace std;

class Date
{
public:
	Date(int year = 1, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	Date(Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}
	bool operator==(Date& d)
	{
		return _year == d._year
			&& _month == d._month
			&& _day == d._day;
	}

	bool operator<(Date& d)
	{
		if (_year == d._year)
		{
			if (_month == d._month)
			{
				if (_day < d._day)
					return true;
			}
			else if (_month < d._month)
				return true;
		}
		else if (_year < d._year)
			return true;
		return false;
	}
	bool operator<=(Date& d)
	{
		return *this == d || *this < d;
	}

	bool operator>(Date& d)
	{
		return !(* this <= d);
	}

	bool operator>=(Date& d)
	{
		return *this > d || *this == d;
	}
	bool operator!=(Date& d)
	{
		return !(*this == d);
	}

	void print()
	{
		cout << _year << "��" << _month << "��" << _day << "��" << endl;
	}

private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date d1(2022, 3 ,1);
	Date d2(2022, 2, 7);
	d1.print();
	d2.print();
	Date d3 = d2;
	d3.print();
	int ret = d3 == d2;
	cout << ret << endl;

	return 0;
}