#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//实现一个函数，打印乘法口诀表，口诀表的行数和列数自己指定
//
//如：输入9，输出9 * 9口诀表，输出12，输出12 * 12的乘法口诀表。
//int main()
//{
//	int row = 0;
//	int col = 0;
//	int input = 0;
//	printf("pls enter n =");
//	scanf("%d", &input);
//	for (row = 1; row <= input; row++)
//	{
//		for (col = 1; col <= row; col++)
//		{
//			int sum = 0;
//			sum = row * col;
//			printf("%-2d*%-2d=%-2d   ", row, col, sum);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//实现一个函数来交换两个整数的内容。
//void swap(int* px, int* py)
//{
//	int z = 0;
//	z = *px;
//	*px = *py;
//	*py = z;
//}
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	printf("please enter a =");
//	scanf("%d", &a);
//	printf("please enter b =");
//	scanf("%d", &b);
//
//	swap(&a, &b);
//
//	printf("a = %d\n", a);
//	printf("b = %d", b);
//
//	return 0;
//}

//实现函数判断year是不是润年。
//int is_leap_year(int x)
//{
//	if (((x % 4 == 0) && (x % 100 != 0)) || (x % 400 == 0))
//		return 1;
//	return 0;
//}
//int main()
//{
//	int y = 0;
//	int count = 0;
//	for (y = 1000; y <= 2000; y++)
//	{
//		if (is_leap_year(y))
//		{
//			count++;
//			printf("%d ", y);
//		}
//	}
//	printf("count = %d", count);
//	return 0;
//}

//实现一个函数，判断一个数是不是素数。
//
//利用上面实现的函数打印100到200之间的素数。

//#include <math.h>
//
//int is_prime(int x)
//{
//	int i = 0;
//	for (i = 2; i <= sqrt(x); i++)
//	{
//		if (x % i == 0)
//			return 0;
//	}
//	return 1;
//}
//
//int main()
//{
//	int n = 0;
//	int count = 0;
//	for (n = 100; n <= 200; n++)
//	{
//		if (is_prime(n))
//		{
//			count++;
//			printf("%d ", n);
//		}
//	}
//	printf("\ncount = %d", count);
//	return 0;
//}

//递归和非递归分别实现求n的阶乘（不考虑溢出的问题）
//递归

//int fac(int n)
//{
//	int ret = 1;
//	if (n > 1)
//		ret = fac(n - 1) * n;
//	else
//		return 1;
//	return ret;
//}
//
//int main()
//{
//	int n = 0;
//	printf("pls enter n =");
//	scanf("%d", &n);
//
//	printf("\nn!=%d\n",fac(n));
//
//	return 0;
//}

//非递归
//int main()
//{
//	int n = 0;
//	printf("pls enter n=");
//	scanf("%d", &n);
//	int i = 0;
//	int ret = 1;
//	for (i = 1; i <= n; i++)
//	{
//		ret = ret * i;
//	}
//	printf("\nn!=%d", ret);
//	return 0;
//}

//递归方式实现打印一个整数的每一位
void print_onebyone(int x)
{
	int r = 0;
	int q = 0;
	r = x % 10;
	q = x / 10;
	if (q != 0)
	{
		print_onebyone(q);
		printf("%d ", r);
	}
	else
	{
		printf("%d ", r);
	}
}

int main()
{
	int input = 0;
	printf("pls enter number=");
	scanf("%d", &input);
	print_onebyone(input);
	return 0;
}