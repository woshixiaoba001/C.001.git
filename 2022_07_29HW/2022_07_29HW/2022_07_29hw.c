#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//不允许创建临时变量，交换两个整数的内容
//int main()
//{
//	int a = 2;
//	int b = 9;
//
//	a = a ^ b;
//	b = a ^ b;
//	a = a ^ b;
//	printf("a=%d b=%d\n", a, b);
//	return 0;
//}
//写一个函数返回参数二进制中 1 的个数。
//
//比如： 15    0000 1111    4 个 1
//int cal_1(int x)
//{
//	int i = 0;
//	int count = 0;
//	for (i = 0; i < 32; i++)
//	{
//		if (x & 1)
//		{
//			count++;
//		}
//		x = x >> 1;
//	}
//	return count;
//}
//
//int main()
//{
//	int input = 0;
//	scanf("%d", &input);
//	printf("%d",cal_1(input));
//
//	return 0;
//}
//编程实现：两个int（32位）整数m和n的二进制表达中，有多少个位(bit)不同？
//
//输入例子 :
//
//1999 2299
//
//输出例子 : 7
int cal_diff(int x, int y)
{
	int tmp = 0;
	tmp = x ^ y;
	int i = 0;
	int count = 0;
	for (i = 0; i < 32; i++)
	{
		if (tmp & 1)
			count++;
		tmp = tmp >> 1;
	}
	return count;
}

int main()
{
	int a = 0;
	int b = 0;
	scanf("%d %d", &a, &b);
	printf("%d",cal_diff(a, b));
	return 0;
}