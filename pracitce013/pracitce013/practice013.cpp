#define _CRT_SECURE_NO_WARNINGS 1
//小乐乐获得4个最大数，请帮他编程找到最大的数。
//输入描述：
//一行，4个整数，用空格分开。
//输出描述：
//一行，一个整数，为输入的4个整数中最大的整数。
int main()
{
    int n = 0;
    int count = 4;
    int max = 0;
    while (count-- && scanf("%d", &n) != EOF)
    {
        if (3 == count)
        {
            max = n;
        }
        else
        {
            if (n > max)
                max = n;
        }
    }
    printf("%d", max);
    return 0;
}