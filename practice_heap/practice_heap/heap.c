#define _CRT_SECURE_NO_WARNINGS 1
#include "heap.h"


void swap(HPDataType* a, HPDataType* b)
{
	HPDataType tmp = *a;
	*a = *b;
	*b = tmp;
}
void adjustdown(HPDataType* a, int n, int parent)
{
	//默认大堆
	//先假设左孩子为最大值
	int child = parent * 2 + 1;
	while (child<n)
	{
		//每一次都要判断左右孩子哪个大
		if (child + 1 < n && a[child + 1] > a[child])
			child++;
		//将大的进行交换
		if (a[child] > a[parent])
		{
			swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
			break;
	}
}
void adjustup(HPDataType* a, int n)
{
	int child = n - 1;
	int parent = (n - 1 - 1) / 2;
	while (child > 0)
	{
		if (a[child] > a[parent])
		{
			swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
			break;
	}
}

/// ///////////////////////////////////////////////////////////////////

void HeapCreate(Heap* hp, HPDataType* a, int n)
{
	assert(hp);
	//给传进来的堆对象中的数组开辟空间
	HPDataType* tmp = 
		(HPDataType*)malloc(sizeof(HPDataType) * n);
	if (tmp == NULL)
	{
		perror("malloc failed");
		exit(-1);
	}
	hp->_a = tmp;
	//将外部数组的数据拷贝到刚刚开辟的空间中
	memcpy(hp->_a, a, sizeof(HPDataType) * n);
	//通过向下调整使堆中数据变成堆的结构
	for (int i = (n-1-1)/2 ; i >= 0; i--)
	{
		adjustdown(hp->_a, n, i);
	}
	hp->_capacity = n;
	hp->_size = n;
}

void HeapDestory(Heap* hp)
{
	assert(hp);
	//销毁后需要在外部将hp置空
	free(hp->_a);
	hp->_capacity = hp->_size = 0;
}

void HeapPush(Heap* hp, HPDataType x)
{
	assert(hp);
	//检查是否需要扩容
	if (hp->_capacity == hp->_size)
	{
		HPDataType* tmp = (HPDataType*)realloc
		(hp->_a, sizeof(HPDataType) * (hp->_capacity) * 2);
		if (tmp == NULL)
		{
			perror("realloc failed");
			exit(-1);
		}
		hp->_a = tmp;
	}
	//插入数据
	hp->_a[hp->_size] = x;
	hp->_size++;
	//对数据向上调整保持为结构为堆
	adjustup(hp->_a, hp->_size);
}

void HeapPop(Heap* hp)
{
	assert(hp);
	assert(!HeapEmpty(hp));
	//先将堆顶与最后一个交换
	swap(&hp->_a[0], &hp->_a[hp->_size - 1]);
	//然后size--，删除最后一个数据
	hp->_size--;
	//然后对堆顶数据向下调整，保持堆结构
	adjustdown(hp->_a, hp->_size, 0);
}

HPDataType HeapTop(Heap* hp)
{
	assert(hp);
	return hp->_a[0];
}

int HeapSize(Heap* hp)
{
	assert(hp);
	return hp->_size;
}

int HeapEmpty(Heap* hp)
{
	assert(hp);
	return hp->_size == 0;
}

void HeapPrint(Heap* hp)
{
	for (int i = 0; i < hp->_size; i++)
	{
		printf("%d ", hp->_a[i]);
	}
	printf("\n");
}

//实现对int类型数据在内存上的TOP k 打印 
void PrintTopK(int* a, int n, int k)
{
	//先在堆上开辟一个K个int大小的空间
	int* heap_k;
	int* tmp = (int*)malloc(sizeof(int) * k);
	if (tmp == NULL)
	{
		perror("malloc failed");
		exit(-1);
	}
	else
	{
		heap_k = tmp;
	}
	//将需要topK的数组的前五个数据先拷贝到这个新空间
	memcpy(heap_k, a, sizeof(int) * k);
	//对k个数据建堆
	for (int i = (k - 1 - 1) / 2; i >= 0; i--)
	{
		adjustdown(heap_k, k, i);
	}
	//然后遍历数组剩余数据将其与堆顶数据比较后，入堆
	for (int j = k; j < n; j++)
	{
		if (a[j] < heap_k[0])
		{
			heap_k[0] = a[j];
			adjustdown(heap_k, k, 0);
		}
	}
	//堆中数据即为TOPk
	for (int i = 0; i < k; i++)
	{
		printf("%d ", heap_k[i]);
	}
	printf("\n");

	free(heap_k);
}

