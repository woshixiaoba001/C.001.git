#define _CRT_SECURE_NO_WARNINGS 1
#include "heap.h"

void test1()
{
	int arr[10] = { 7,3,5,9,1,8,6,4,0,2 };
	Heap h1;
	HeapCreate(&h1, arr, 10);
	HeapPrint(&h1);
	HeapPush(&h1, 100);
	HeapPrint(&h1);
	printf("%d \n",HeapSize(&h1));
	printf("%d \n",HeapEmpty(&h1));
	HeapPop(&h1);
	HeapPop(&h1);
	HeapPop(&h1);
	HeapPop(&h1);
	HeapPrint(&h1);
	printf("%d \n",HeapTop(&h1));
	HeapDestory(&h1);
}

void TestTopk()
{
	int a1[10] = { 1,99,45,78,88,3,2,0,9,10 };
	PrintTopK(a1, 10, 5);

	srand(time(NULL));
	int a[1000];
	for (time_t i = 0; i < 1000; i++)
	{
		a[i] = rand() % 1000 + 1;
	}
	PrintTopK(a, 1000, 5);

}

int main()
{
	test1();
	TestTopk();
	return 0;
}