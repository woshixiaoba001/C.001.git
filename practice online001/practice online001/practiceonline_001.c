#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//键盘输入一个字符串，编写代码获取字符串的长度并输出，要求使用字符指针实现。
int main()
{
    char arr[100] = { 0 };
    int i = 0;
    while (scanf("%c", &arr[i]) != EOF)
    {
        i++;
    }
    printf("%d", i - 1);
    return 0;
}