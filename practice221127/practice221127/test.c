﻿#define _CRT_SECURE_NO_WARNINGS 1

//给你一个长度为 n 的链表，每个节点包含一个额外增加的随机指针 random ，该指针可以指向链表中的任何节点或空节点。
//
//构造这个链表的 深拷贝。 深拷贝应该正好由 n 个 全新 节点组成，其中每个新节点的值都设为其对应的原节点的值。新节点的 next 指针和 random 指针也都应指向复制链表中的新节点，并使原链表和复制链表中的这些指针能够表示相同的链表状态。复制链表中的指针都不应指向原链表中的节点 。
//
//来源：力扣（LeetCode）
//链接：https ://leetcode.cn/problems/copy-list-with-random-pointer
//著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。

    struct Node 
     {
     int val;
     struct Node *next;
     struct Node *random;
  };

#include <stdlib.h>
#include <assert.h>
struct Node* copyRandomList(struct Node* head)
{
    //将复制的节点先插入对应的原节点后面，并链接成一个链表
    struct Node* cur = head;
    struct Node* copy;
    while (cur)
    {
        struct Node* next = cur->next;
        struct Node* tmp = (struct Node*)malloc(sizeof(struct Node));
        if (tmp == NULL)
        {
            perror("malloc error:");
            exit(-1);
        }
        copy = tmp;
        copy->val = cur->val;
        cur->next = copy;
        copy->next = next;

        cur = next;
    }
    //对copy里的random进行赋值；
    cur = head;
    while (cur)
    {
        if (cur->random == NULL)
        {
            cur->next->random = NULL;
        }
        else
        {
            cur->next->random = cur->random->next;
        }
        cur = cur->next->next;
    }
    //解绑节点，并分别进行尾插
    cur = head;
    struct Node* copyhead = NULL;
    struct Node* copytail = NULL;
    while (cur)
    {
        copy = cur->next;
        struct Node* next = cur->next->next;
        cur->next = next;
        if (copytail == NULL)
        {
            copyhead = copy;
            copytail = copy;
        }
        else
        {
            copytail->next = copy;
            copytail = copytail->next;
        }
        cur = next;
    }

    return copyhead;
}