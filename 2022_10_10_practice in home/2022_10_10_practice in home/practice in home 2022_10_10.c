#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

void triangle(int row)
{
	int arr[30][30] = {0};
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j <= i; j++)
		{
			if ((j == 0)||(j == i))
				 arr[i][j] = 1;
			if (j > 0 && j < i && i > 1)
			{
				arr[i][j] = arr[i - 1][j - 1] + arr[i - 1][j];
			}
		}		
	}
	
	for (i = 0; i < row; i++)
	{
		for (j = 0; j <= i; j++)
		{
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
}

int main()
{
	int n = 0;
	scanf("%d", &n);	
	triangle(n);	
	return 0;
}