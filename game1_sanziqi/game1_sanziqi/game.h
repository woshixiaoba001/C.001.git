#pragma once
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define ROW 5
#define COL 5

void menu(void);

void initial_board(char arr[ROW][COL], int row, int col);

void display_board(char arr[ROW][COL], int row, int col);

void player_move(char arr[ROW][COL], int row, int col);

void computer_move(char arr[ROW][COL], int row, int col);

char is_win(char arr[ROW][COL], int row, int col);




