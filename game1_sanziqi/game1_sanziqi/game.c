#define _CRT_SECURE_NO_WARNINGS 1
#include "game.h"

void menu()
{
	printf("******************\n");
	printf("***** 1.play *****\n");
	printf("***** 0.exit *****\n");
	printf("******************\n");
}

void initial_board(char arr[ROW][COL], int row, int col)
{
	int i = 0;
	int j = 0;

	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			arr[i][j]=' ';
		}
	}

}

void display_board(char arr[ROW][COL], int row, int col)
{
	int i = 0;
	int j = 0;
	int x = 0;

	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			if (j < col - 1)
			{
				printf(" %c |", arr[i][j]);
			}
			else
			{
				printf(" %c \n", arr[i][j]);
			}	
		}
		if(i<row-1)
		{
			for (x = 0; x < col; x++)
			{
				if (x < col - 1)
				{
					printf("---|");
				}
				else
				{
					printf("---\n");
				}
			}
		}		
	}
}

void player_move(char arr[ROW][COL], int row, int col)
{
	while (1)
	{
		int a = 0;
		int b = 0;
		scanf("%d %d", &a, &b);
		if (0 < a && a < (row + 1) && 0 < b && b < (col + 1))
		{
			if (arr[a - 1][b - 1] == ' ')
			{
				arr[a - 1][b - 1] = '*';
					break;
			}
			else
			{
				printf("已被占用，请重新输入\n");
			}
		}
		else
		{
			printf("输入错误，请重新输入\n");
		}
	}
}

void computer_move(char arr[ROW][COL], int row, int col)
{
	//while (1)
	//{
	//	int a = rand() % row;
	//	int b = rand() % col;

	//	if (arr[a][b] == ' ')
	//	{
	//		arr[a][b] = '#';
	//		break;
	//	}
	//}
	int a = rand() % 8+1;
	int i = 0;
	int j = 0;
	int count = 0;


	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			if (arr[i][j] == '*')
			{
				count++;
				if (count == 1)
				{
					switch (a)
					{
					case 1:
						arr[i - 1][j] = '#';
						break;
					case 2:
						arr[i - 1][j - 1] = '#';
						break;
					case 3:
						arr[i][j - 1] = '#';
						break;
					case 4:
						arr[i + 1][j - 1] = '#';
						break;
					case 5:
						arr[i + 1][j] = '#';
						break;
					case 6:
						arr[i + 1][j + 1] = '#';
						break;
					case 7:
						arr[i][j + 1] = '#';
						break;
					case 8:
						arr[i + 1][j + 1] = '#';
						break;
					}
				}
				else if(count == 2)
				{

				}
				}
			}
		}
	}


}

static int is_full(char arr[ROW][COL], int row, int col)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			if (arr[i][j] == ' ')
				return 0;
		}
	}
	return 1;
}

char is_win(char arr[ROW][COL], int row, int col)
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		if (arr[i][0] == arr[i][1] && arr[i][1] == arr[i][2] && arr[i][0] != ' ')
		return arr[i][0];
	}
	for (i = 0; i < col; i++)
	{
		if (arr[0][i] == arr[1][i] && arr[1][i] == arr[2][i] && arr[0][i] != ' ')
		return arr[0][i];
	}
	if (arr[0][0] == arr[1][1] && arr[1][1] == arr[2][2] && arr[1][1] != ' ')
	return arr[1][1];
	if (arr[0][2] == arr[1][1] && arr[1][1] == arr[2][0] && arr[1][1] != ' ')
	return arr[1][1];

	if (is_full(arr, row, col) == 1)
		return 'p';
	
	return 'c';
}