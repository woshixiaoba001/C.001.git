#define _CRT_SECURE_NO_WARNINGS 1
#include "game.h"

void game()
{
	char ret = ' ';
	char board[ROW][COL] = { 0 };
	initial_board(board, ROW, COL);
	display_board(board, ROW, COL);
	while (1)
	{	
		printf("玩家走\n");
		player_move(board, ROW, COL);
		display_board(board, ROW, COL);

		ret = is_win(board, ROW, COL);
		if ( ret != 'c')
			break;

		printf("电脑走\n");
		computer_move(board, ROW, COL);
		display_board(board, ROW, COL);

		ret = is_win(board, ROW, COL);
		if ( ret != 'c')
			break;
	}
	if (ret == '*')
		printf("玩家获胜\n");
	else if (ret == '#')
		printf("电脑获胜\n");
	else if (ret == 'p')
		printf("平局\n");
	display_board(board, ROW, COL);

		
}

int main()
{
	srand((unsigned int)time(NULL));
	int input = 0;
	do
	{
		menu();
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("输入错误，请重新输入\n");
			break;
		}

	} while (input);

	return 0;
}