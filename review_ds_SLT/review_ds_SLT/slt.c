#include "slt.h"

SLT* buynewnode(SLTdatatype x)
{
	SLT* newnode = (SLT*)malloc(sizeof(SLT));
	if (newnode == NULL)
	{
		perror("malloc failed");
		exit(-1);
	}
	newnode->next = NULL;
	newnode->val = x;

	return newnode;
}

SLT* createSLT(int num)
{
	SLT* head;
	SLT* tail = head = NULL;
	for (int i = 0; i < num; i++)
	{
		if (head == NULL)
		{
			head = tail = buynewnode(i);
		}
		else
		{
			tail->next = buynewnode(i);
			tail = tail->next;
		}
	}
	return head;
}

void print(SLT* phead)
{
	SLT* cur = phead;
	while (cur)
	{
		printf("%d ", cur->val);
		cur = cur->next;
	}
	printf("\n");
}

void SLTpushback(SLT** pphead, SLTdatatype x)
{
	if (*pphead == NULL)
	{
		*pphead = buynewnode(x);
	}
	else
	{
		SLT* tail = *pphead;
		while (tail->next)
		{
			tail = tail->next;
		}
		tail->next = buynewnode(x);
	}
}

void SLTpopback(SLT** pphead)
{
	assert(*pphead);
	SLT* tail;
	SLT* prev = tail = *pphead;
	if (tail->next == NULL)
	{
		free(tail);
		*pphead = NULL;
	}
	else 
	{
		while (tail->next)
		{
			prev = tail;
			tail = tail->next;
		}
		free(tail);
		prev->next = NULL;
	}
}

void SLTpushfront(SLT** pphead, SLTdatatype x)
{
	SLT* newnode = buynewnode(x);
	newnode->next = *pphead;
	*pphead = newnode;
}

void SLTpopfront(SLT** pphead)
{
	assert(*pphead);
	if ((*pphead)->next == NULL)
	{
		free(*pphead);
		*pphead = NULL;
	}
	else
	{
		SLT* next = (*pphead)->next;
		free(*pphead);
		*pphead = next;
	}
}


SLT* SLTfind(SLT* pphead, SLTdatatype x)
{
	SLT* cur = pphead;
	while (cur)
	{
		if (cur->val == x)
			return cur;
		cur = cur->next;
	}
	return NULL;
}

void SLTinsertAfter(SLT* pos, SLTdatatype x)
{
	if (pos == NULL)
	{
		return;
	}
	else 
	{
		SLT* newnode = buynewnode(x);
		SLT* next = pos->next;
		pos->next = newnode;
		newnode->next = next;
	}
}

void SLTinsert(SLT** pphead, SLT* pos, SLTdatatype x)
{
	assert(pos);
	SLT* newnode = buynewnode(x);
	SLT* prev = *pphead;
	if ((*pphead)==pos)
	{
		SLTpushfront(pphead, x);
	}
	else 
	{
		while (prev->next != pos)
		{
			prev = prev->next;
		}
		prev->next = newnode;
		newnode->next = pos;
	}
}

void SLTdestroy(SLT** pphead)
{
	SLT* cur = *pphead;
	while (cur)
	{
		SLT* prev = cur;
		cur = cur->next;
		free(prev);
	}
	*pphead = NULL;
}




