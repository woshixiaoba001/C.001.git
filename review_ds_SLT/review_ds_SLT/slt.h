#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int SLTdatatype;

typedef struct SLTnode 
{
	SLTdatatype val;
	struct SLTnode* next;
}SLT;

SLT* buynewnode(SLTdatatype x);
SLT* createSLT(int num);
void print(SLT* phead);

void SLTpushback(SLT** pphead, SLTdatatype x);
void SLTpopback(SLT** pphead);

void SLTpushfront(SLT** pphead, SLTdatatype x);
void SLTpopfront(SLT** pphead);

SLT* SLTfind(SLT* pphead, SLTdatatype x);

void SLTinsertAfter(SLT* pos, SLTdatatype x);
void SLTeraseAfter(SLT* pos);

void SLTinsert(SLT** pphead, SLT* pos, SLTdatatype x);
void SLTerase(SLT** pphead, SLT* pos);

void SLTdestroy(SLT** pphead);
