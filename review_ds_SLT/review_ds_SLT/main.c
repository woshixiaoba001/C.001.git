#define _CRT_SECURE_NO_WARNINGS 1
#include "slt.h"

void test1()
{
	SLT* s1;
	s1 = createSLT(10);
	print(s1);
}

void test2()
{
	SLT* s2 = createSLT(3);
	SLTpushback(&s2, 10);
	SLTpushback(&s2, 100);
	SLTpushback(&s2, 1000);
	SLTpopback(&s2);
	SLTpopback(&s2);
	SLTpopback(&s2);
	SLTpopback(&s2);
	SLTpopback(&s2);
	SLTpopback(&s2);
	print(s2);
}

void test3()
{
	SLT* s3 = NULL;
	SLTpushfront(&s3, 1);
	SLTpushfront(&s3, 2);
	SLTpushfront(&s3, 3);
	SLTpushfront(&s3, 4);
	print(s3);
	//SLTpopfront(&s3);
	//SLTpopfront(&s3);
	//SLTpopfront(&s3);
	//SLTpopfront(&s3);
	//SLTpopfront(&s3);
	SLT* pos = NULL;
	pos = SLTfind(s3, 4);
	//print(s3);
	//SLTinsertAfter(NULL, 100);
	SLTinsert(&s3, pos, 100);
	print(s3);

}



int main()
{
	//test1();
	//test2();
	test3();

	return 0;
}