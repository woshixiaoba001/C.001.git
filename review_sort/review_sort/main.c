#define _CRT_SECURE_NO_WARNINGS 1
#include <stdlib.h>
#include <stdio.h>

void swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}
void print(int* arr)
{
	for (int i = 0; i < 10; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");

}

//int sort(int* a, int begin, int end)
//{
//	int prev = begin;
//	int cur = begin + 1;
//	int key = begin;
//	while (cur <= end)
//	{
//		if (a[cur] < &a[key])
//		{
//			prev++;
//			swap(&a[cur], &a[prev]);
//		}
//		cur++;
//	}
//	swap(&a[prev], a[key]);
//	key = prev;
//	return key;
//}



void insert_sort(int* arr, int n)
{

	for(int i = 0; i<n-1; i++)
	{
		int end = i;
		int tmp = arr[end + 1];
		while (end>=0)
		{
			if (tmp < arr[end])
			{
				arr[end + 1] = arr[end];
				end--;
			}
			else
				break;
		}
		arr[end + 1] = tmp;
	}

}

void shellsort(int* arr, int n)
{
//	int gap = 3;
//for (int j = 0; j < gap; ++j)
//{
//	for (int i = j; i < n - gap; i += gap)
//	{
//		int end = i;
//		int tmp = arr[end + gap];
//		while (end >= 0)
//		{
//			if (tmp < arr[end])
//			{
//				arr[end + gap] = arr[end];
//				end -= gap;
//			}
//			else
//			{
//				break;
//			}
//		}
//
//		arr[end + gap] = tmp;
//	}
//}
//insert_sort(arr, 10);
	
	int gap = 3;
	while (gap > 1)
	{
		gap = gap / 2;
	


		for (int i=0; i < n - gap; i++)
		{
			int end = i;
			int tmp = arr[end + gap];
			while (end >= 0)
			{
				if (tmp < arr[end])
				{
					arr[end + gap] = arr[end];
					end = end - gap;
				}
				else
					break;
			}
			arr[end + gap] = tmp;
		}
	}
	
}


void adjustdown(int* arr, int n, int parent)//默认大堆
{
	//假设左孩子为大的
	int child = parent * 2 + 1;

	while (child < n)
	{
		//每次判断是否交换前，要先判断哪个孩子为大的
		if (child + 1 < n && arr[child + 1] > arr[child])
		{
			child++;
		}
		//如果孩子大于父节点，则交换，并将下标调整
		if (arr[child] > arr[parent])
		{
			swap(&arr[child], &arr[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
			break;
	}
}

void heap_sort(int* arr, int n)
{
	//将数组最后一个数的父节点作为起始节点，开始向下调整建堆
	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
	{
		adjustdown(arr, n, i);
	}
	//然后将堆顶元素与数组最后一个元素交换，
	//然后再次向下调整，保持堆的结构，并使堆的结构减小1个单位
	int end = n - 1;
	while (end)
	{
		swap(&arr[0], &arr[end]);
		adjustdown(arr, end, 0);
		end--;
	}
}

void select_sort(int* arr, int n)
{
	//int begin = 0;
	//int end = n - 1;

	//while (begin < end)
	//{
	//	int min = begin;
	//	int max = begin;
	//	for (int i = begin + 1; i <= end; i++)
	//	{
	//		if (arr[i] < arr[min])
	//		{
	//			min = i;
	//		}
	//		if (arr[i] > arr[max])
	//		{
	//			max = i;
	//		}
	//	}
	//	swap(&arr[begin], &arr[min]);
	//	if (begin == max)
	//		max = min;
	//	swap(&arr[end], &arr[max]);

	//	begin++;
	//	end--;
	//}

	int begin = 0;

	while (begin < n - 1)
	{
		int min = begin;
		for (int i = begin + 1; i < n; i++)
		{
			if (arr[i] < arr[min])
				min = i;
		}
		swap(&arr[begin], &arr[min]);
		begin++;
	}
}






int main()
{
	int arr1[10] = {10,9,8,7,6,5,4,3,2,1};
	int arr2[10] = { 10,9,8,7,6,5,4,3,2,1 };
	int arr3[10] = { 10,9,8,7,6,5,4,3,2,1 };

	//insert_sort(arr1, 10);
	print(arr1);

	//shellsort(arr2, 10);
	print(arr2);

	heap_sort(arr3, 10);
	//select_sort(arr3, 10);
	print(arr3);


	return 0;
}
