#define _CRT_SECURE_NO_WARNINGS 1
#include "stack.h"

void STinitial(ST* ps)
{
	assert(ps);
	Stackdatatype* a = (Stackdatatype*)malloc(sizeof(Stackdatatype) * 4);
	if (a == NULL)
	{
		perror("malloc failed");
		exit(-1);
	}
	ps->a = a;
	ps->capacity = 4;
	ps->top = 0;
	ps->size = 0;
}

void STdestroy(ST* ps)
{
	assert(ps);
	free(ps->a);
	ps->capacity = ps->top = ps->size = 0;
	ps = NULL;
}

void STpush(ST* ps, Stackdatatype x)
{
	assert(ps);
	if (ps->top == ps->capacity)
	{
		int newcapacity = ps->capacity * 2;
		Stackdatatype* tmp = (Stackdatatype*)realloc(ps->a, sizeof(Stackdatatype) * newcapacity);
		if (tmp == NULL)
		{
			perror("realloc failed");
			exit(-1);
		}
		ps->a = tmp;
		ps->capacity = newcapacity;
	}
	ps->a[ps->top] = x;
	ps->top++;
	ps->size++;
}

void STpop(ST* ps)
{
	assert(ps);
	assert(!ST_is_empty(ps));
	ps->top--;
	ps->size--;
}

Stackdatatype STtop(ST* ps)
{
	assert(ps);
	assert(!ST_is_empty(ps));
	return ps->a[ps->top - 1];
}

bool ST_is_empty(ST* ps)
{
	assert(ps);
	return ps->top == 0;
}

int ST_size(ST* ps)
{
	assert(ps);
	return ps->size;
}






