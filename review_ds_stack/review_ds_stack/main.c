#define _CRT_SECURE_NO_WARNINGS 1
#include "stack.h"

void test1()
{
	ST s1;
	STinitial(&s1);
	STpush(&s1, 1);
	STpush(&s1, 2);
	STpush(&s1, 3);
	STpush(&s1, 4);
	STpush(&s1, 5);
	STpop(&s1);
	STpop(&s1);
	STpop(&s1);
	STpop(&s1);
	STpop(&s1);
	STpop(&s1);
}

void test2()
{
	ST s2;
	STinitial(&s2);
	STpush(&s2, 1);
	STpush(&s2, 2);
	STpush(&s2, 3);
	STpush(&s2, 4);
	STpush(&s2, 5);
	while (!ST_is_empty(&s2))
	{
		int ret = STtop(&s2);
		printf("%d ", ret);
		STpop(&s2);
	}
	printf("\n");
}

int main()
{
	//test1();
	test2();
	return 0;
}