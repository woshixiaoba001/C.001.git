#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

typedef int Stackdatatype;

typedef struct STLstack
{
	Stackdatatype* a;
	int capacity;
	int top;
	int size;
}ST;

void STinitial(ST* ps);
void STdestroy(ST* ps);

void STpush(ST* ps, Stackdatatype x);
void STpop(ST* ps);
Stackdatatype STtop(ST* ps);

bool ST_is_empty(ST* ps);
int ST_size(ST* ps);



