#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdbool.h>


//KiKi知道了什么是质数（只能被1和他自身整除的数），他现在想知道所有三位整数中，有多少个质数。
bool isprime(int x) {
    for (int i = 2; i < x; i++) {
        if (x % i == 0) {
            return false;
        }
    }
    return true;
}
int main() {
    int i = 100;
    int count = 0;
    for (i = 100; i < 1000; i++) {
        if (isprime(i))
            count++;
    }
    printf("%d", count);
    return 0;
}