#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//牛牛正在寄快递，他了解到快递在 1kg 以内的按起步价 20 元计算，超出部分按每 kg 1元计算，不足 1kg 部分按 1kg计算。如果加急的话要额外付五元，请问牛牛总共要支付多少快递费
//输入描述：
//第一行输入一个单精度浮点数 a 和一个字符 b ，a 表示牛牛要寄的快递的重量，b表示牛牛是否选择加急，'y' 表示加急 ，'n' 表示不加急。
//输出描述：
//输出牛牛总共要支付的快递费用
int main()
{
    float a = 0.0f;
    char b = '\0';
    scanf("%f %c", &a, &b);
    int sum = 0;
    if (a <= 1.0)
    {
        sum = 20;
    }
    else
    {
        int tmp = (a - 1.0) * 10;
        if (0 == tmp % 10)
            sum = tmp / 10 + 20;
        else
            sum = tmp / 10 + 20 + 1;
    }
    if ('y' == b)
        sum += 5;

    printf("%d", sum);

    return 0;
}