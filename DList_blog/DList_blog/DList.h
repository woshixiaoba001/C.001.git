#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int DataType;

typedef struct DList
{
	struct DList* prev; //指向上一个节点的指针
	struct DList* next; //指向下一个节点的指针
	DataType val;
}DL;

//生成一个节点声明
DL* BuyNode(DataType X);

//生成一个初始化的带头双向循环链表
DL* InitialDList();

//销毁的声明
void Destroy(DL* guard);

//打印的声明
void DListPrint(DL* guard);

//尾插的声明
void DListPushBack(DL* guard, DataType X);
//尾删的声明
void DListPopBack(DL* guard);

//头插的声明
void DListPushFront(DL* guard, DataType X);
//头删的声明
void DListPopFront(DL* guard);

//查询数据，返回其节点地址的声明
DL* DListFind(DL* guard, DataType X);

//在pos的之前插入数据的声明
void DListInsertbefore(DL* pos, DataType X);

//删除pos位置的节点的声明
void DListErase(DL* pos);