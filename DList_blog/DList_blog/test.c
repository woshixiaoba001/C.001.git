#define _CRT_SECURE_NO_WARNINGS 1
#include "DList.h"
int main()
{
	DL* GUARD = InitialDList();
	DListPushBack(GUARD, 1);
	DListPushBack(GUARD, 10);
	DListPushBack(GUARD, 100);
	DListPushBack(GUARD, 1000);
	DListPushBack(GUARD, 10000);

	DListPopBack(GUARD);
	DListPopBack(GUARD);

	DListPushFront(GUARD, 5);
	DListPushFront(GUARD, 55);
	DListPushFront(GUARD, 555);

	DL* ret = DListFind(GUARD, 555);
	DListInsertbefore(ret, 999);
	DListErase(ret);
	DListPrint(GUARD);
	return 0;
}