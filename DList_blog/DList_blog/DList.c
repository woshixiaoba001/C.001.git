#define _CRT_SECURE_NO_WARNINGS 1
#include "DList.h"

//生成一个节点的实现
DL* BuyNode(DataType X)
{
	DL* newnode = NULL;
	DL* tmp = (DL*)malloc(sizeof(DL));
	if (tmp == NULL)
	{
		perror("BuyNode fail:");
		exit(-1);
	}
	newnode = tmp;
	newnode->next = NULL;
	newnode->prev = NULL;
	newnode->val = X;
	return newnode;
}

//创建一个初始化的带头双向循环链表的实现
DL* InitialDList()
{
	DL* guard = BuyNode(-1); //可放任何值
	guard->next = guard;
	guard->prev = guard;
	return guard;
}

//销毁的实现
void Destroy(DL* guard) //使用此函数后记得需要将guard置空，同free一样的用法
{
	assert(guard);
	DL* head = guard->next;
	while (head!=guard)
	{
		DL* next = head->next;
		free(head);
		head = next;
	}
	free(guard);
}

//打印的实现
void DListPrint(DL* guard)
{
	assert(guard);
	DL* head = guard->next;
	while (head != guard)
	{
		printf("%d ", head->val);
		head = head->next;
	}
}

//尾插的实现
void DListPushBack(DL* guard, DataType X)
{
	assert(guard);
	DL* newnode = BuyNode(X);
	DL* prev = guard->prev;
	prev->next = newnode;
	newnode->next = guard;
	guard->prev = newnode;
	newnode->prev = prev;
}
//尾删的实现
void DListPopBack(DL* guard)
{
	assert(guard);
	assert(guard->prev != guard);//不能把哨兵位删除
	DL* tail = guard->prev;
	DL* tailprev = tail->prev;
	guard->prev = tailprev;
	tailprev->next = guard;
	free(tail);
}

//头插的实现
void DListPushFront(DL* guard, DataType X)
{
	assert(guard);
	DL* newnode = BuyNode(X);
	DL* head = guard->next;
	guard->next = newnode;
	newnode->next = head;
	head->prev = newnode;
	newnode->prev = guard;
}
//头删的实现
void DListPopFront(DL* guard)
{
	assert(guard);
	assert(guard->next != guard);//不能把哨兵位删除
	DL* head = guard->next;
	DL* next = head->next;
	guard->next = next;
	next->prev = guard;
	free(head);
}

//查询数据，返回其节点地址的实现
DL* DListFind(DL* guard, DataType X)
{
	assert(guard);
	DL* head = guard->next;
	while (head != guard)
	{
		if (head->val == X)
		{
			return head;
		}
		head = head->next;
	}
	return NULL;
}

//在pos的之前插入数据的实现
void DListInsertbefore(DL* pos, DataType X)
{
	assert(pos);
	DL* newnode = BuyNode(X);
	DL* prev = pos->prev;
	prev->next = newnode;
	newnode->next = pos;
	pos->prev = newnode;
	newnode->prev = prev;
}

//删除pos位置的节点的实现
void DListErase(DL* pos)
{
	assert(pos);
	assert(pos->val!=-1);//防止误删哨兵头节点
	DL* prev = pos->prev;
	DL* next = pos->next;
	free(pos);
	prev->next = next;
	next->prev = prev;
}