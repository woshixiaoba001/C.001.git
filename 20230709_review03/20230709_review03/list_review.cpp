#include <iostream>
#include <cstdbool>
using namespace std;

namespace xboll
{
	template<class T>
	struct list_node
	{
		typedef list_node<T> node;
		node* _prev;
		node* _next;
		T _data;

		list_node(const T& n = T())
			:_prev(nullptr)
			, _next(nullptr)
			, _data(n)
		{}
	};

	template<class T>
	struct list_iterator
	{
		typedef list_iterator<T> self;
		typedef list_node<T> node;
		node* _node;

		list_iterator(node* n)
			:_node(n)
		{}

		T& operator*()
		{
			return _node->_data;
		}

		self& operator++()
		{
			_node = _node->_next;
			return *this;
		}

		bool operator!=(const self& n)
		{
			return _node != n._node;
		}

	};

	template<class T>
	class list
	{
		typedef list_node<T> node;
	public:
		typedef list_iterator<T> iterator;

		list()
		{
			_head = new node;
			_head->_next = _head;
			_head->_prev = _head;
		}
		void push_back(const T& x)
		{
			node* tmp = new node(x);
			node* tail = _head->_prev;

			tmp->_prev = tail;
			tail->_next = tmp;
			tmp->_next = _head;
			_head->_prev = tmp;
		}
		iterator begin()
		{
			return iterator(_head->_next);
		}

		iterator end()
		{
			return iterator(_head);
		}

	private:
		node* _head;
	};

}


int main()
{
	xboll::list<int> test;
	test.push_back(1);
	test.push_back(2);
	test.push_back(3);
	test.push_back(4);
	for (auto& e : test)
	{
		cout << e*2 << " ";
	}
	cout << endl;


	return 0;
}