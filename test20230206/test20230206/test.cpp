#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>

int add(int& x, int& y) 
{
	return x + y;
}

int main() 
{
	int a = 2;
	int b = 3;
	std::cout << add(a, b) << std::endl;

	return 0;
}