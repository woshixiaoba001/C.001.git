#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//输入描述：
//多组输入，每行输入一个字母。
//输出描述：
//针对每组输入，输出为一行，如果输入字母是元音（包括大小写），输出“Vowel”，如果输入字母是非元音，输出“Consonant”。

int main()
{
    char n = 0;
    while (scanf("%c", &n) != EOF)
    {
        getchar();

        if (n == 'a' || n == 'A' || n == 'e' || n == 'E' || n == 'O' || n == 'o' || n == 'U' || n == 'u' || n == 'I' || n == 'i')
            printf("Vowel\n");
        else
            printf("Consonant\n");
    }
    return 0;
}