#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//键盘输入一个长度为len（1 <= len < 30）的字符串，再输入一个正整数 m（1 <= m <= len），将此字符串中从第 m 个字符开始的剩余全部字符复制成为另一个字符串，并将这个新字符串输出。要求用指针处理字符串。
int main()
{
    char arr1[100] = { 0 };
    char arr2[100] = { 0 };
    int n = 0;
    int i = 0;
    scanf("%99s", arr1);
    scanf("%d", &n);

    while (arr1[n - 1 + i] != '\0')
    {
        arr2[i] = arr1[n - 1 + i];
        i++;
    }
    printf("%s", arr2);
}