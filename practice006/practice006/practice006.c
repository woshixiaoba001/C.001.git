#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <math.h>
//小乐乐喜欢数字，尤其喜欢0和1。他现在得到了一个数，想把每位的数变成0或1。如果某一位是奇数，就把它变成1，如果是偶数，那么就把它变成0。请你回答他最后得到的数是多少。
//输入描述：
//输入包含一个整数n(0 ≤ n ≤ 109)
//输出描述：
//输出一个整数，即小乐乐修改后得到的数字。
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//
//    int ret = 0;
//    int i = 0;
//    while (n)
//    {
//        int w = n % 10;
//        if (0 == w % 2)
//        {
//            w = 0;
//        }
//        else
//        {
//            w = 1;
//        }
//        ret += w * pow(10, i++);
//        n /= 10;
//    }
//
//    printf("%d\n", ret);
//
//    return 0;
//}

int change(int n)
{
    int count = 0;
    int arr[1000] = { 0 };
    while (n)
    {
        if (n % 10 % 2 == 0)
        {
            arr[count] = 0;
            count++;
        }
        else
        {
            arr[count] = 1;
            count++;
        }
        n = n / 10;
    }

    int i = 0;
    int sum = 0;
    for (i = 0; i <= count; i++)
    {
        sum += (int)arr[i] * pow(10, i);
    }
    return (int)sum;
}

int main()
{
    int n = 0;
    int ret = 0;
    scanf("%d", &n);
    ret = change(n);

    printf("%d", ret);

    return 0;
}