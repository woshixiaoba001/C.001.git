#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
//假设0为结构体的起始地址，
//对其强制类型转换为结构体指针类型，然后对成员访问并对其取地址，
//得到地址是相对于起始地址0的变化，实质为偏移量
//再将地址强制类型转换为unsigned int
//#define MY_OFFSETOF(type, member) (size_t)&(((type*)0)->member)
//
//struct student
//{
//	int i;
//	int j;
//	char name[20];
//	char sex[10];
//	int age;
//	char address[5];
//};
//int main()
//{
//	printf("%d\n", offsetof(struct student, j));
//	printf("%d\n", MY_OFFSETOF(struct student, j));
//	printf("%d\n", offsetof(struct student, sex));
//	printf("%d\n", MY_OFFSETOF(struct student, sex));
//	return 0;
//}

//首先将数据与01010101 01010101 01010101 01010101按位与，得到奇数位的信息
//再用左移操作符，将奇数位全部移动到偶数位
//首先将数据与10101010 10101010 10101010 10101010按位与，得到偶数位的信息
//再用右移操作符，将偶数位全部移动到奇数位。
//最后两个相加，即为二进制奇偶位交换的数
#define B_EXCHANGE(X) (((X&0X55555555)<<1)+((X&0XAAAAAAAA)>>1))
int main()
{
	int i = 170;
	int ret = B_EXCHANGE(i);
	printf("%d", ret);
	return 0;
}