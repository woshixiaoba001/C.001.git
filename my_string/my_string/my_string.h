#pragma once
#include <iostream>
#include <cstring>
#include <cassert>

namespace xboll
{
	class My_String
	{
	public:
		//构造、析构、拷贝构造、赋值重载
		My_String(const char* s = "")
			:_size(strlen(s))
		{
			_str = new char[_size + 1];
			_capacity = _size;
			if (_capacity == 0)
				_capacity = 4;
			strcpy(_str, s);
		}

		~My_String()
		{
			delete[] _str;
			_str = nullptr;
			_capacity = _size = 0;
		}

		My_String(const My_String& s)
		{
			_size = s._size;
			_capacity = s._capacity;
			_str = new char[_capacity + 1];
			strcpy(_str, s._str);
		}

		My_String& operator=(const My_String& s)
		{
			if (this != &s)
			{
				char* tmp = new char[s._capacity + 1];
				strcpy(tmp, s._str);
				delete[] _str;
				_str = tmp;
				_size = s._size;
				_capacity = s._capacity + 1;
			}
			return *this;
		}
		//迭代器
		typedef char* iterator;

		iterator begin()
		{
			return _str;
		}

		iterator end()
		{
			return _str + _size;
		}

		size_t size() const
		{
			return _size;
		}
		//运算符重载
		char& operator[](size_t index)
		{
			assert(index >= 0 && index < _size);
			return *(_str + index);
		}
		const char& operator[](size_t index) const
		{
			assert(index >= 0 && index < _size);
			return *(_str + index);
		}
		//容量
		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;
				_capacity = n;
			}
		}
		//修改
		void push_back(char c)
		{
			if (_size + 1 > _capacity)
			{
				reserve(_capacity*2);
			}
				_str[_size] = c;
				_size++;
				_str[_size] = '\0';
		}

	private:
		char* _str;
		size_t _size;
		size_t _capacity;
	};
}
