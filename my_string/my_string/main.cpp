#define _CRT_SECURE_NO_WARNINGS 1
#include "my_string.h"

using namespace std;


void test1()
{
	xboll::My_String s1;
	xboll::My_String s2("woshixiaoba");

	cout<< s1.size() << endl;
	cout<< s2.size() << endl;
	xboll::My_String s3 = s2;
	for (auto& c : s3)
	{
		cout << c << ' ';
	}
	cout << endl;
	s2.push_back('A');
	s2.push_back('b');
	s2.push_back('c');
	for (auto& c : s2)
	{
		cout << c << ' ';
	}
}

int main()
{
	test1();
	return 0;
}