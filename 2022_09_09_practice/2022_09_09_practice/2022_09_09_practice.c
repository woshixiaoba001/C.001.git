#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>

//模拟实现strlen
//size_t my_strlen(const char* arr)
//{
//	int i = 0;
//	while (*(arr+i) != '\0')
//	{
//		i++;
//	}
//	return i;
//}
//
//int main()
//{
//	char ch[] = "abc\0adfadf";
//	printf("%zu",my_strlen(ch));
//	return 0;
//}

//模拟实现strcpy
//char* my_strcpy(char* des, const char* src)
//{
//	char* tmp = des;
//	while (*tmp++ = *src++)
//	{
//		;
//	}
//	return des;
//}
//
//int main()
//{
//	char arr1[] = "xxxxxxxxxx";
//	char arr2[] = "hello\0vvv";
//	
//	printf("%s", my_strcpy(arr1, arr2));
//	return 0;
//}

//模拟实现strcmp
//int my_strcmp(const char* arr1, const char* arr2)
//{
//	while (*arr1 == *arr2)
//	{
//		if (*arr1 == '\0')
//		{
//			return 0;
//		}
//		arr1++;
//		arr2++;
//	}
//	return (*arr1 - *arr2);
//}
//
//int main()
//{
//	char arr1[] = "ab";
//	char arr2[] = "abA";
//	int ret = 0;
//	ret = my_strcmp(arr1, arr2);
//	printf("%d", ret);
//	return 0;
//}

//模拟实现strcat
//char* my_strcat(char* des,const char* src)
//{
//	char* tmp = des;
//	while (*tmp != '\0')
//	{
//		tmp++;
//	}
//	while (*tmp++ = *src++)
//	{
//		;
//	}
//	return des;
//}
//
//int main()
//{
//	char arr1[100] = "woshi ";
//	char arr2[] = "xiaoba ";
//	char arr3[] = "001";
//	my_strcat(arr1, arr2);
//	printf("%s",my_strcat(arr1, arr3));
//	return 0;
//}

//模拟实现strstr
const char* my_strstr(const char* arr1, const char* arr2)
{
	const char* a1 = arr1;
	const char* a2 = arr2;
	const char* reset = a1;
	
	while (reset)
	{
		reset = a1;
		while (*a1 == *a2 && *a1 != '\0' && *a2 != '\0')
		{
			a1++;
			a2++;
		}
		if (*a2 == '\0')
			return  reset;
		else
		{
			a1 = reset + 1;
			a2 = arr2;
		}
	}
	return NULL;
}

int main()
{
	char arr1[] = "helloworld";
	char arr2[] = "lowo";
	printf("%s",my_strstr(arr1, arr2));
	return 0;
}