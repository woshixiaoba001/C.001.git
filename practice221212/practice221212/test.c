#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
//描述
//牛牛从键盘输入了一个长度为 n 的数组，把这个数组转换成链表然后把链表中所有值是 x 的节点都删除。
//输入描述：
//第一行输入两个正整数 n 和 x 表示数组的长度和要删除的链表节点值 x 。
//第二行输入 n 个正整数表示数组中每个元素的值。
//输出描述：
//把数组转换成链表然后删除所有值是 x 的节点，删除后输出这个链表。
typedef struct SLNode {
    int val;
    struct SLNode* next;
}SL;

SL* BuyNode(int x) {
    SL* newnode = (SL*)malloc(sizeof(SL));
    if (newnode == NULL) {
        perror("buynode fail:");
        exit(-1);
    }
    newnode->next = NULL;
    newnode->val = x;
    return newnode;
}

int main() {
    int n = 0;
    scanf("%d", &n);
    int arr[n];
    int count = n;
    int del = 0;
    scanf("%d", &del);
    int i = 0;
    while (count--) {

        scanf("%d", &arr[i]);
        i++;
    }
    SL* guardhead = (SL*)malloc(sizeof(SL));
    if (guardhead == NULL) {
        perror("buynode fail:");
        exit(-1);
    }
    guardhead->next = NULL;
    guardhead->val = -1;
    SL* tail = guardhead;
    i = 0;
    count = n;
    while (count--) {
        tail->next = BuyNode(arr[i]);
        tail = tail->next;
        i++;
    }

    SL* cur = guardhead->next;
    SL* prev = cur;
    while (cur) {
        if (cur->val == del) {
            if (cur == prev) {
                guardhead->next = cur->next;
                free(cur);
            }
            else {
                SL* delnode = cur;
                cur = cur->next;
                prev->next = cur;
                free(delnode);
            }
        }
        else {
            prev = cur;
            cur = cur->next;
        }
    }
    cur = guardhead->next;
    while (cur) {
        printf("%d ", cur->val);
        cur = cur->next;
    }
    return 0;
}