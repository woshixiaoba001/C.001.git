#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//小S：终于可以开学啦！好开心啊！
//小Y：你没看新闻吗，开学日期又延后了。
//小S：NOOOOOOOOOOOOOOOO！
//小S知道原计划星期X开学，通知开学时间延期N天，请问开学日期是星期几（星期日用7表示）？
//输入描述：
//输入包含两个数字X，N（1≤X≤7, 1≤N≤1000）。
//输出描述：
//输出一个数字，表示开学日期是星期几。
int main()
{
    int x = 0;
    int n = 0;
    scanf("%d %d", &x, &n);
    printf("%d", ((x + n) % 7 == 0) ? 7 : (x + n) % 7);
    return 0;
}