#define _CRT_SECURE_NO_WARNINGS 1
#include "heap.h"

int main() {
	H heap;
	int random[10] = { 45,62,89,12,4,68,74,84,22,30 };
	int n = sizeof(random) / sizeof(int);
	heap_create(&heap, random, n);
	printf("%d\n",heap_top(&heap));
	printf("%d\n",is_heap_empty(&heap));
	printf("%d\n", heap_size(&heap));
	heap_pop(&heap);
	printf("%d\n", heap_top(&heap));
	printf("%d\n", heap_size(&heap));
	heap_print(&heap);
	heap_destroy(&heap);
	return 0;
	//H heap;
	//heap_initial(&heap);
	//printf("%d\n", is_heap_empty(&heap));
	//for (int i = 0; i < sizeof(random) / sizeof(int); i++) {
	//	heap_push(&heap, random[i]);
	//}
	//heap_sort(random, n);
	//int i = 0;
	//while (i!=10) {
	//	printf("%d ", random[i]);
	//	i++;
	//}
}

