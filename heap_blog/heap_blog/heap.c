#define _CRT_SECURE_NO_WARNINGS 1
#include "heap.h"

void heap_initial(H* ph) {
	assert(ph);
	ph->arr = NULL;
	ph->size = 0;
	ph->capacity = 0;
}

void heap_create(H* ph, HeapDataType* arr, int n) {
	int child = n - 1;
	int parent = (child - 1) / 2;

	HeapDataType* tmp = (HeapDataType*)malloc(sizeof(HeapDataType) * n);
	if (tmp == NULL) {
		perror("malloc fail:");
		exit(-1);
	}
	ph->arr = tmp;
	ph->size = ph->capacity = n;
	memmove(ph->arr, arr, sizeof(HeapDataType) * n);
	//�����㷨
	while (parent >= 0) {
		adjustdown(ph->arr, n, parent);
		parent--;
	}
}

void heap_destroy(H* ph) {
	assert(ph);
	free(ph->arr);
	ph->arr = NULL;
	ph->size = 0;
	ph->capacity = 0;
}

void heap_print(H* ph) {
	assert(ph);
	for (int i = 0; i < ph->size; i++) {
		printf("%d ", ph->arr[i]);
	}
}

void swap(HeapDataType* x, HeapDataType* y) {
	HeapDataType tmp = *x;
	*x = *y;
	*y = tmp;
}

void adjustup(H* ph, int child) {
	int parent = (child - 1) / 2;
	while (child>0) {
		if (ph->arr[child] > ph->arr[parent]) {
			swap(&ph->arr[child], &ph->arr[parent]);
		}
		child = parent;
		parent = (child - 1) / 2;
	}
}

void adjustdown(HeapDataType* arr, int n, int parent) {
	int child = parent * 2 + 1;
	while (child<n) {
		if (child + 1 < n && arr[child + 1] > arr[child]) {
			child = parent * 2 + 2;
		}
		if (arr[child] > arr[parent]) {
			swap(&arr[child], &arr[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else {
			break;
		}
	}	
}

void heap_push(H* ph, HeapDataType x) {
	assert(ph);
	if (ph->size == ph->capacity) {
		int newcapacity = ph->capacity == 0 ? 4 : ph->capacity * 2;
		HeapDataType* tmp = (HeapDataType*)realloc(ph->arr, newcapacity * sizeof(HeapDataType));
		if (tmp == NULL) {
			perror("realloc fail:");
			exit(-1);
		}
		ph->arr = tmp;
		ph->capacity = newcapacity;
	}
	ph->arr[ph->size] = x;
	ph->size++;
	adjustup(ph, ph->size-1);
}

void heap_pop(H* ph) {
	assert(ph);   
	swap(&ph->arr[0], &ph->arr[ph->size - 1]);
	ph->size--;
	adjustdown(ph->arr,ph->size,0);
}

HeapDataType heap_top(H* ph) {
	assert(ph);
	assert(ph->size > 0);
	return ph->arr[0];
}

bool is_heap_empty(H* ph) {
	assert(ph);
	return ph->size == 0;
}

int heap_size(H* ph) {
	assert(ph);
	return ph->size;
}

void heap_sort(int* arr, int n) {
	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
		adjustdown(arr, n, i);
	int end = n - 1;
	while (end) {
		swap(&arr[0], &arr[end]);
		adjustdown(arr, end, 0);
		end--;
	}
}
