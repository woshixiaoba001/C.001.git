#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

typedef int HeapDataType;

typedef struct Heap {
	HeapDataType* arr;
	int size;
	int capacity;
}H;
//创建一个堆的结构
void heap_initial(H* ph);
//销毁一个堆
void heap_destroy(H* ph);
//打印一个堆
void heap_print(H* ph);
//堆的插入数据
void heap_push(H* ph, HeapDataType x);
//堆的删除数据
void heap_pop(H* ph);
//堆顶数据
HeapDataType heap_top(H* ph);
//判断堆是否为空
bool is_heap_empty(H* ph);
//堆中的元素个数
int heap_size(H* ph);
//向下调整
void adjustdown(HeapDataType* arr, int n, int parent);
//向上调整
void adjustup(H* ph, int child);

//将数组转换成堆
void heap_create(H* ph, HeapDataType* arr, int n);
//堆排序
void heap_sort(int* arr, int n);
