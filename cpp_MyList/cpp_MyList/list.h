#pragma once
#include <iostream>

namespace xboll
{
	template <class T>
	struct List_Node
	{
		List_Node<T>* _prev;
		List_Node<T>* _next;
		List_Node<T> _val;

		List_Node(const List_Node<T>& x = T())
			:_prev(nullptr)
			,_next(nullptr)
			,_val(x)
		{}
	};

	template<class T>
		class My_List
	{
	public:
		My_List()
			:_head->_prev = _head
			,_head->_next = _head
			,_head->_val = 
		{}
	private:
		List_Node<T>* _head;
	};

}
