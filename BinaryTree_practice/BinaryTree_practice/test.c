#define _CRT_SECURE_NO_WARNINGS 1
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

typedef struct binarytree
{
	int val;
	struct binarytree* left;
	struct binarytree* right;
}BT;

int treeheight(BT* root)
{
	if (root == NULL)
	{
		return 0;
	}
	int big = treeheight(root->left);
	int small = treeheight(root->right);
	if(big < small)
	{
		int tmp = big;
		big = small;
		small = tmp;
	}
	return big + 1;
}

bool single_val_tree(BT* root)
{
	if (root == NULL)
		return true;
	if (root->left && root->val != root->left->val)
		return false;
	if (root->right && root->val != root->right->val)
		return false;
	return single_val_tree(root->left) && single_val_tree(root->right);
}

bool is_same_tree(BT* t1, BT* t2)
{
	if (t1 == NULL && t2 == NULL)
		return true;
	if (t1 == NULL || t2 == NULL)
		return false;
	if (t1->val != t2->val)
		return false;
	
	return is_same_tree(t1->left, t2->left) && is_same_tree(t1->right, t2->right);
}


BT* buynode(int x)
{
	BT* root = NULL;
	BT* tmp = (BT*)malloc(sizeof(BT));
	if (tmp == NULL)
	{
		perror("malloc failed");
		exit(-1);
	}
	root = tmp;
	root->val = x;
	root->left = NULL;
	root->right = NULL;
}

int main()
{
	BT* n1 = buynode(1);
	BT* n2 = buynode(2);
	BT* n3 = buynode(3);
	BT* n4 = buynode(4);
	BT* n5 = buynode(5);
	n1->left = n2;
	n1->right = n3;
	n2->left = n4;
	n4->left = n5;
	printf("%d \n",treeheight(n1));
	printf("%d \n", single_val_tree(n1));
	return 0;
}