#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
//描述
//牛牛尝试把一个长度为 n 的数组转换成链表并把链表前两个节点交换位置和把链表最后两个节点交换位置。
//输入描述：
//第一行输入一个正整数 n 表示数组的长度
//第二行输入 n 个正整数，表示数组中各个元素的值
//输出描述：
//把数组转换成链表后输出交换位置后的链表
//typedef struct SL
//{
//    int val;
//    struct SL* next;
//}SL;
//
//SL* BuyNode(int X)
//{
//    SL* newnode = (SL*)malloc(sizeof(SL));
//    if (newnode == NULL)
//    {
//        perror("BuyNode fail:");
//        exit(-1);
//    }
//    newnode->next = NULL;
//    newnode->val = X;
//    return newnode;
//}
//
//int main()
//{
//    int n = 0;
//    //建立哨兵位
//    SL* guardhead = (SL*)malloc(sizeof(SL));
//    if (guardhead == NULL)
//    {
//        perror("guardhead inital fail:");
//        exit(-1);
//    }
//    SL* tail = guardhead;
//    guardhead->next = NULL;
//    guardhead->val = -1;
//    scanf("%d", &n);
//    int val = 0;
//    //输入链表数据
//    while (n--)
//    {
//        scanf("%d", &val);
//        tail->next = BuyNode(val);
//        tail = tail->next;
//    }
//    //交换前两个节点
//    SL* prev = guardhead->next;
//    SL* cur = prev->next;
//    SL* next = cur->next;
//    guardhead->next = cur;
//    cur->next = prev;
//    prev->next = next;
//    //交换后两个节点
//    cur = guardhead->next;
//    next = cur->next;
//    while (next->next)
//    {
//        prev = cur;
//        cur = cur->next;
//        next = cur->next;
//    }
//    prev->next = next;
//    next->next = cur;
//    cur->next = NULL;
//    //打印交换后的链表
//    cur = guardhead->next;
//    while (cur)
//    {
//        printf("%d ", cur->val);
//        cur = cur->next;
//    }
//    return 0;
//
//}

typedef struct BTNode
{
    int val;
    struct BTNode* left;
    struct BTNode* right;
}BTNode;
BTNode*  buynode(int x)
{
    BTNode* newnode = (BTNode*)malloc(sizeof(BTNode));
    if (newnode == NULL)
    {
        perror("malloc fail:");
        exit(-1);
    }
    BTNode* btn = newnode;
    btn->val = x;
    btn->left = NULL;
    btn->right = NULL;
    return btn;
};
int treeHeight(BTNode* root);

int main()
{
    BTNode* n1 = buynode(1);
    BTNode* n2 = buynode(2);
    BTNode* n3 = buynode(3);
    BTNode* n4 = buynode(4);
    BTNode* n5 = buynode(5);
    BTNode* n6 = buynode(6);
    BTNode* n7 = buynode(7);

    n1->left = n2;
    n1->right = n4;
    n2->left = n3;
    n4->left = n5;
    n4->right = n6;
    n6->left = n7;

    printf("%d ", treeHeight(n1));
    return 0;
}
int treeHeight(BTNode* root)
{
    int big = 0;
    int small = 0;
    if (root == NULL)
    {
        return 0;
    }
    
    big = treeHeight(root->left);
    small = treeHeight(root->right);
    if (big < small)
    {
        int tmp = big;
        big = small;
        small = tmp;
    }

    return big+1;
}

