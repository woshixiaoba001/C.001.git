#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
//描述
//牛牛输入了一个长度为 n 的数组，他想把这个数组转换成链表，链表上每个节点的值对应数组中一个元素的值，然后遍历链表并求和各节点的值。
//输入描述：
//第一行输入一个正整数 n ，表示数组的长度。
//第二行输入 n 个正整数，表示数组中各个元素的值。
//输出描述：
//把数组转换成链表然后对其求和并输出这个值。


typedef struct SLnode {
    int val;
    struct SLnode* next;
}SL;

SL* buynode(int x) {
    SL* newnode = (SL*)malloc(sizeof(SL));
    if (newnode == NULL)
    {
        perror("buynode fail:");
        exit(-1);
    }
    newnode->val = x;
    newnode->next = NULL;
    return newnode;
}
int main() {
    int n = 0;
    scanf("%d", &n);
    int arr[n];
    int i = 0;
    int count = n;
    while (count--)
    {
        scanf("%d", &arr[i]);
        i++;
    }
    SL* guardhead = (SL*)malloc(sizeof(SL));
    if (guardhead == NULL)
    {
        perror("malloc fail:");
        exit(-1);
    }
    SL* tail = guardhead;
    count = n;
    i = 0;
    while (count--)
    {
        tail->next = buynode(arr[i]);
        tail = tail->next;
        i++;
    }
    SL* cur = guardhead->next;
    int sum = 0;
    while (cur)
    {
        sum = sum + cur->val;
        cur = cur->next;
    }
    printf("%d", sum);
    return 0;
}