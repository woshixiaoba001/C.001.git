#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//#define row 3
//#define col 10
//
//
//void find_n(int x, int (*arr)[col])
//{
//	int i = 0;
//	int j = 0;
//	int tag = 1;
//	if (x <= arr[0][col - 1])
//	{
//		for (i = 0; i < col ; i++)
//		{
//			if (x == arr[0][i])
//			{
//				printf("found %d \nrow 1 col %d\n", x, i + 1);
//				tag = 0;
//				break;
//			}
//		}				
//	}
//	else if (x >= arr[row - 1][0])
//	{
//		for (i = 0; i < col ; i++)
//		{
//			if (x == arr[row - 1][i])
//			{
//				printf("found %d \nrow %d col %d\n", x, row,i+1);
//				tag = 0;
//				break;
//			}
//		}	
//	}
//	else if (x > arr[0][col - 1] && x < arr[row - 1][0])
//	{
//		for (i = 1; i < row - 1; i++)
//		{
//			if (arr[i][0] < x && x < arr[i][col - 1])
//			{
//				for (j = 0; j < col-1; j++)
//				{
//					if (x == arr[i][j])
//					{
//						printf("found %d \nrow %d col %d\n", x, i+1, j+1);
//						tag = 0;
//						break;
//					}
//				}
//			}
//		}
//	}
//	if(tag)
//	printf("not found %d\n", x);
//}
//
//int main()
//{
//	int arr[row][col] = { 0 };
//	int i = 0;
//	int j = 0;
//	int spawn = 1;
//	for (i = 0; i < row; i++)
//	{
//		for (j = 0; j < col; j++)
//		{
//			arr[i][j] = spawn;
//			spawn++;
//		}
//	}
//
//	for (i = 0; i < row; i++)
//	{
//		for (j = 0; j < col; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//
//	int n = 0;
//	while (scanf("%d", &n) != EOF)
//	{
//		find_n(n, arr);
//	}
//	return 0;
//}

//实现一个函数，可以左旋字符串中的k个字符。
//
//
//例如：
//
//
//ABCD左旋一个字符得到BCDA
//
//ABCD左旋两个字符得到CDAB
//#include <string.h>
//
//void rota_str(char* arr, int len, int k)
//{
//	char tmp[50] = { 0 };
//	int i = 0;
//	int j = 0;
//	for (i = k; i < len; i++)
//	{
//		tmp[j] = arr[i];
//		j++;
//	}
//	for (i = 0; i < k; i++)
//	{
//		tmp[j] = arr[i];
//		j++;
//	}
//	printf("%s\n", tmp);
//}
//
//int main()
//{
//	char input[50] = { 0 };
//	int k = 0;
//
//	printf("输入字符串\n");
//	gets(input);
//	printf("输入想左旋字符个数\n");
//	scanf("%d", &k);
//	int len = strlen(input);
//	rota_str(input, len,k);
//
//	return 0;
//}
//
//写一个函数，判断一个字符串是否为另外一个字符串旋转之后的字符串。
#include <string.h>

int is_rota_pair(char* arr1, char* arr2, int len1, int len2)
{
	char tmp[50] = { 0 };
	int k = 0;
	int i = 0;
	int j = 0;
	int tag = 1;
	int count = 0;
	if (len1 != len2)
		return 0;
	else
	{
		for (k = 1; k < len1; k++)
		{
			for (i = k; i < len1; i++)
			{
				tmp[j] = arr1[i];
				j++;
			}
			for (i = 0; i < k; i++)
			{
				tmp[j] = arr1[i];
				j++;
			}
			j = 0;
			count = 0;
			for (i = 0; i < len1; i++)
			{
				if (tmp[i] == arr2[i])
				{
					count++;
				}
			}
			if (count == len1)
				tag = 0;			
		}
	}
	if (tag)
		return 0;
	else
		return 1;
}

int main()
{
	char arr1[50] = { 0 };
	char arr2[50] = { 0 };
	while (scanf("%s %s", &arr1, &arr2) != EOF)
	{
		int len1 = strlen(arr1);
		int len2 = strlen(arr2);

		printf("%d", is_rota_pair(arr1, arr2, len1, len2));
	}
	
	return 0;
}