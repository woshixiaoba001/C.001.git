#define _CRT_SECURE_NO_WARNINGS 1
#include "SeqList.h"

//动态顺序表的初始化的实现
void Initial_SL(SL* psl)
{
	assert(psl);
	psl->arr = NULL;
	psl->size = 0;
	psl->capacity = 0;
}

//动态顺序表的显示的实现
void Print_SL(SL* psl)	
{
	assert(psl);
	int i = 0;
	for (i = 0; i < psl->size; i++)
	{
		printf("%d ", psl->arr[i]);
	}
	printf("\n");
}

//动态顺序表的扩容检查的实现
void Check_SLcapacity(SL* psl)
{
	assert(psl);
	if (psl->size == psl->capacity)
	{
		size_t newcapacity = 0;
		newcapacity = (psl->capacity) == 0 ? 4 : psl->capacity * 2;
		DataType* tmp = (DataType*)realloc(psl->arr, newcapacity * sizeof(DataType));
		if (tmp == NULL)
		{
			perror("Check_SLcapacity");
			exit(-1);
		}
		psl->arr = tmp;
		psl->capacity = newcapacity;
	}
}

//动态顺序表的销毁的实现
void Destroy_SL(SL* psl)
{
	assert(psl);
	if (psl->arr) 
	{
		free(psl->arr);
		psl->capacity = 0;
		psl->size = 0;
		psl->arr = NULL;
	}
}

//尾插的实现
void PushBack(SL* psl, DataType x)
{
	assert(psl);
	Check_SLcapacity(psl);
	psl->arr[psl->size] = x;
	psl->size++;
}
//尾删的实现
void PopBack(SL* psl)
{	
	assert(psl);
	assert(psl->size > 0);
	psl->size--;
}

//头插的实现
void PushFront(SL* psl, DataType x)
{
	assert(psl);
	Check_SLcapacity(psl);
	//从最后一个开始一个一个往后挪动
	int end = psl->size - 1;//注意end这个下标的临时变量只能用int，不能用size_t
	while (end >= 0)
	{
		psl->arr[end + 1] = psl->arr[end];
		end--;
	}
	//插入数据，并增加元素数量
	psl->arr[0] = x;
	psl->size++;
}
//头删的实现
void PopFront(SL* psl)
{
	assert(psl);
	assert(psl->arr);
	size_t start = 1;
	while (start < psl->size)
	{
		psl->arr[start - 1] = psl->arr[start];
		start++;
	}
	psl->size--;
}

//pos处插入数据的实现
void SLInsert(SL* psl, int pos, DataType x)
{
	assert(psl);
	assert(pos >= 0);//可以等于0；因为是任意pos插入，也包括了头插的情况。
	assert(pos <= psl->size);//可以等于size，因为是任意pos插入，也包括了尾插的情况
	Check_SLcapacity(psl);
	int end = psl->size - 1;
	while (end >= pos)
	{
		psl->arr[end + 1] = psl->arr[end];
		end--;
	}
	psl->arr[pos] = x;
	psl->size++;
}
//pos处删除数据的实现
void SLErase(SL* psl, int pos)
{
	assert(psl);
	assert(pos >= 0);
	assert(pos < psl->size);
	int start = pos;
	while (start < psl->size-1)
	{
		psl->arr[start] = psl->arr[start + 1];
		start++;
	}
	psl->size--;
}

//从begin位置出开始查找相应元素下标
int SLFind(SL* psl, DataType x, int s)
{
	int i = 0;
	for (i = 0; i < psl->size; i++)
	{
		if (x == psl->arr[i])
			return i;	
	}
	return -1;
}

