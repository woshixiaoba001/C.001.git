#pragma once
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

typedef	int DataType;//方便不同类型数据使用顺序表

//动态顺序表的结构
typedef struct SeqList
{
	DataType* arr;
	size_t size;
	size_t capacity;
}SL;

//动态顺序表的初始化的声明
void Initial_SL(SL* psl);

//动态顺序表的显示的声明
void Print_SL(SL* psl);

//动态顺序表的扩容检查的声明
void Check_SLcapacity(SL* psl);

//动态顺序表的销毁的声明
void Destroy_SL(SL* psl);

//尾插的声明
void PushBack(SL* psl, DataType x);
//尾删的声明
void PopBack(SL* psl);

//头插的声明
void PushFront(SL* psl, DataType x);
//头删的声明
void PopFront(SL* psl);

//pos处插入数据的声明
void SLInsert(SL* psl, int pos, DataType x);
//pos处删除数据的声明
void SLErase(SL* psl, int pos);

//从begin位置处开始查找相应元素下标 
int SLFind(SL* psl, DataType x, int begin);
