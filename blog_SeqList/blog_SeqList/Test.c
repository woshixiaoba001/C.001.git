#define _CRT_SECURE_NO_WARNINGS 1
#include "SeqList.h"

void test1()
{
	SL tsl;
	Initial_SL(&tsl);
	PushFront(&tsl, 1);
	PushFront(&tsl, 2);
	PushFront(&tsl, 3);
	PushFront(&tsl, 4);
	PushFront(&tsl, 5);
	PushFront(&tsl, 6);
	PushFront(&tsl, 7);
	SLInsert(&tsl, 3, 888);
	SLInsert(&tsl, 0, 888);
	SLInsert(&tsl, 9, 888);
	//将顺序表中相同的元素全部删除
	int pos = 0;
	pos = SLFind(&tsl, 888, 0);
	while (pos != -1)
	{
		SLErase(&tsl, pos);
		pos = SLFind(&tsl, 888, pos);
	}	
	Print_SL(&tsl);
	Destroy_SL(&tsl);
}

int main()
{	
	test1();
	return 0;
}