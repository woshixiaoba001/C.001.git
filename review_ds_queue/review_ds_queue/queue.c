#define _CRT_SECURE_NO_WARNINGS 1
#include "queue.h"

void Qinitial(Q* pq)
{
	assert(pq);
	pq->head = NULL;
	pq->tail = NULL;
	pq->size = 0;
}

void Qdestroy(Q* pq)
{
	
}

void Qpush(Q* pq, QueueDatatype x)
{
	assert(pq);
	Qnode* newnode = (Qnode*)malloc(sizeof(Qnode));
	if (newnode == NULL)
	{
		perror("malloc failed");
		exit(-1);
	}
	if (pq->size == 0)
	{
		pq->head = pq->tail = newnode;
	}
	else
	{
		pq->tail->next = newnode;
		pq->tail = pq->tail->next;
	}
	pq->size++;
	newnode->next = NULL;
	newnode->val = x;
}

void Qpop(Q* pq)
{
	assert(pq);
	assert(!Q_isempty(pq));
	if (pq->head == pq->tail)
	{
		free(pq->head);
		pq->head = NULL;
		pq->tail = NULL;
	}
	else 
	{
		Qnode* del = pq->head;
		pq->head = pq->head->next;
		free(del);
	}
	pq->size--;
}

QueueDatatype Qfront(Q* pq)
{
	assert(pq);
	assert(!Q_isempty(pq));
	return pq->head->val;
}

QueueDatatype Qback(Q* pq)
{
	assert(pq);
	assert(!Q_isempty(pq));
	return pq->tail->val;
}



bool Q_isempty(Q* pq)
{
	assert(pq);
	return pq->size == 0;
}
int Q_size(Q* pq)
{
	assert(pq);
	return pq->size;
}





