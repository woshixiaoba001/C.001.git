#define _CRT_SECURE_NO_WARNINGS 1
#include "queue.h"

void test1()
{
	Q q1;
	Qinitial(&q1);
	Qpush(&q1, 1);
	Qpush(&q1, 2);
	Qpush(&q1, 3);
	Qpush(&q1, 4);
	printf("%d \n",Qback(&q1));
	printf("%d \n", Q_size(&q1));
	printf("%d \n", Q_isempty(&q1));

	while (!Q_isempty(&q1))
	{
		int ret = Qfront(&q1);
		printf("%d ", ret);
		Qpop(&q1);
	}
	printf("\n");

}

int main()
{
	test1();
	return 0;
}