#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//牛牛从键盘输入整数 x 和左右边界 l 和 r 共三个整数。请你判断 x 是否在 l 和 r 之间 （即是否存在 l≤x≤r l≤x≤r ）
int main()
{
    int x = 0;
    int l = 0;
    int r = 0;
    scanf("%d %d %d", &x, &l, &r);
    if (x >= l && x <= r)
        printf("true\n");
    else
        printf("false\n");
    return 0;
}