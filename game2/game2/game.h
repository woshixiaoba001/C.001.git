#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ROW 11
#define COL 11
#define ROWS ROW-2
#define COLS COL-2
#define MINE_NUM 10

void initial_board(char arr[ROW][COL], int row, int col, char initial);

void display_board(char arr[ROW][COL], int row, int col);

void set_mine(char arr[ROW][COL], int rows, int cols);

void find_mine(char arr1[ROW][COL],char arr2[ROW][COL], int rows, int cols);


