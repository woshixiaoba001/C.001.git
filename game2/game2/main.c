#define _CRT_SECURE_NO_WARNINGS 1
#include "game.h"
void game()
{
	char mine[ROW][COL] = { 0 };
	char show[ROW][COL] = { 0 };
	initial_board(show, ROW, COL, '*');
	initial_board(mine, ROW, COL, '0');
	set_mine(mine, ROWS, COLS);
	display_board(show, ROWS, COLS);
	find_mine(mine, show, ROWS, COLS);
	display_board(mine, ROWS, COLS);
}

void menu()
{
	printf("*******************\n");
	printf("***** 1.start *****\n");
	printf("*****  0.end  *****\n");
	printf("*******************\n");

}

int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请选择\n");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("数据错误，请重新输入\n");
			break;
		}
	} while (input);

	return 0;
}




