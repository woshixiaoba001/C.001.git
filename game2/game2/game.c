#define _CRT_SECURE_NO_WARNINGS 1
#include "game.h"

void initial_board(char arr[ROW][COL], int row, int col, char initial)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			arr[i][j] = initial;
		}
	}
}

void display_board(char arr[ROW][COL], int rows, int cols)
{
	int i = 0;
	int j = 0;
	for (j = 0; j < rows+1; j++)
	{
		printf("%d ", j);
	}
	printf("\n");
	for (i = 1; i <= rows; i++)
	{
		printf("%d ", i);
		for (j = 1; j <= cols; j++)
		{
			printf("%c ", arr[i][j]);
		}
		printf("\n");
	}
}

void set_mine(char arr[ROW][COL], int rows, int cols)
{
	int a = 0;
	int b = 0;
	int count = MINE_NUM;
	while (count)
	{
		a = rand() % 9 + 1;
		b = rand() % 9 + 1;	
		if (arr[a][b] != '1')
		{
			arr[a][b] = '1';
			count--;
		}
	}
}

void find_mine(char arr1[ROW][COL],char arr2[ROW][COL], int rows, int cols)
{
	int a = 0;
	int b = 0;
	int left = rows * cols - MINE_NUM;
	while(left)
	{
		printf("请输入坐标:");
		scanf("%d %d", &a, &b);
		if (a >= 1 && a <= 9 && b >= 1 && b <= 9)
		{
			if (arr2[a][b] == '*')
			{
				if (arr1[a][b] != '1')
				{
					arr2[a][b] = (arr1[a - 1][b] + arr1[a - 1][b - 1] + 
									arr1[a][b - 1] + arr1[a + 1][b - 1] + 
										arr1[a + 1][b] + arr1[a + 1][b + 1] + 
								arr1[a][b + 1] + arr1[a - 1][b + 1]) - 8 * '0' + '0';
					left--;
					display_board(arr2, ROWS, COLS);
				}
				else
				{
					printf("boom！！！你死了\n");
					break;
				}
			}
			else
			{
				printf("此坐标已扫描过，请重新输入\n");
			}
			
		}
		else
		{
			printf("坐标超出范围，请重新输入\n");
		}
	}
	if (0 == left)
	{
		printf("排雷成功，游戏结束\n");
	}	
}

