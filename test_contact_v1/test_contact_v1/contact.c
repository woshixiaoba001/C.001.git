#define _CRT_SECURE_NO_WARNINGS 1
#include "contact.h"
//初始化通讯录函数实现
void initial_contact(struct contact* list)
{
	assert(list);
	memset(list->arrcon, 0, sizeof(struct peopleinfo) * MAX);
	list->num = 0;
	return;
}
//打印通讯录函数实现
void show_contact(const struct contact* list)
{
	assert(list);
	printf("%-20s %-10s %-5s %-11s %-20s\n", "姓名","性别","年龄","电话","住址");
	for (int i = 0; i < list->num; i++)
	{
		printf("%-20s %-10s %-5d %-11s %-20s\n", list->arrcon[i].name,
			list->arrcon[i].sex, list->arrcon[i].age,
			list->arrcon[i].phone, list->arrcon[i].address);
	}
}
//在通讯录添加一个人员信息的函数实现
void add_contact(struct contact* list)
{
	assert(list);
	if (list->num == MAX)
		printf("通讯录已满，无法添加新成员\n");
	else
	{
		printf("请输入姓名\n");
		scanf("%s", list->arrcon[list->num].name);
		printf("请输入性别\n");
		scanf("%s", list->arrcon[list->num].sex);
		printf("请输入年龄\n");
		scanf("%d", &(list->arrcon[list->num].age));
		printf("请输入电话\n");
		scanf("%s", list->arrcon[list->num].phone);
		printf("请输入住址\n");
		scanf("%s", list->arrcon[list->num].address);
		list->num++;
		printf("添加新成员成功\n");
	}
}
//根据姓名查找人
static int findbyname(const struct contact* list, const char* del)
{
	assert(list&&del);
	int ret = 1;
	for (int i = 0; i < list->num; i++)
	{
		ret = strcmp(list->arrcon[i].name, del);
		if (0 == ret)
			return i;
	}
	return -1;
}
//在通讯录中删除指定人的信息的函数实现
void del_contact(struct contact* list)
{
	assert(list);
	char del[NAME] = { 0 };
	int tag = 0;
	printf("请输入要删除人的姓名\n");
	while (1)
	{
		scanf("%s", del);
		tag = findbyname(list, del);
		if (tag != -1 )
			break;
		else
			 printf("无此人信息,请重新输入\n");
	}
	for (int i = tag; i < (list->num)-1; i++)
	{
		list->arrcon[i] = list->arrcon[i + 1];
	}
	list->num--;
	printf("成功删除此人\n");
}
//在通讯录查找指定人信息的函数实现
void search_contact(const struct contact* list)
{
	assert(list);
	char del[NAME] = { 0 };
	int tag = 0;
	printf("请输入要查找人的姓名\n");
	while (1)
	{
		scanf("%s", del);
		tag = findbyname(list, del);
		if (tag != -1)
			break;
		else
			printf("无此人信息,请重新输入\n");
	}
	printf("%-20s %-10s %-5s %-11s %-20s\n", "姓名", "性别", "年龄", "电话", "住址");
	printf("%-20s %-10s %-5d %-11s %-20s\n", list->arrcon[tag].name,
		list->arrcon[tag].sex, list->arrcon[tag].age,
		list->arrcon[tag].phone, list->arrcon[tag].address);
}
//在通讯录更改指定人信息的函数实现
void change_contact(struct contact* list)
{
	assert(list);
	char del[NAME] = { 0 };
	int tag = 0;
	printf("请输入要更改信息人的姓名\n");
	while (1)
	{
		scanf("%s", del);
		tag = findbyname(list, del);
		if (tag != -1)
			break;
		else
			printf("无此人信息,请重新输入\n");
	}
	printf("请输入新姓名\n");
	scanf("%s", list->arrcon[tag].name);
	printf("请输入新性别\n");
	scanf("%s", list->arrcon[tag].sex);
	printf("请输入新年龄\n");
	scanf("%d", &(list->arrcon[tag].age));
	printf("请输入新电话\n");
	scanf("%s", list->arrcon[tag].phone);
	printf("请输入新住址\n");
	scanf("%s", list->arrcon[tag].address);
	printf("信息更改成功\n");
}
//根据姓名对通讯录排序
int cmpname(void* e1, void* e2)
{
	return *(((struct peopleinfo*)e1)->name) - *(((struct peopleinfo*)e2)->name);
}

void sortbyname(struct contact* list)
{
	assert(list);
	qsort(list->arrcon, list->num, sizeof(struct peopleinfo), cmpname);
}