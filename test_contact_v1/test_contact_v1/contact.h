#pragma once
#include <stdio.h>
#include <string.h>
#include <assert.h>

#define NAME 20
#define SEX 10
#define PHONE 11
#define ADDRESS 20
#define MAX 100

//人的信息
struct peopleinfo
{
	char name[NAME];
	char sex[SEX];
	int age;
	char phone[PHONE];
	char address[ADDRESS];
};
//通讯录
struct contact
{
	struct peopleinfo arrcon[MAX];
	int num;
};
//方便使用Switch而设置枚举
enum choice
{
	exit,
	add,
	del,
	search,
	change,
	show,
	cleanall,
	sort,
};
//初始化通讯录函数声明
void initial_contact(struct contact* list);
//打印通讯录函数声明
void show_contact(const struct contact* list);
//在通讯录添加一个人员信息的函数声明
void add_contact(struct contact* list);
//在通讯录中删除指定人的信息的函数声明
void del_contact(struct contact* list);
//在通讯录查找指定人信息的函数声明
void search_contact(const struct contact* list);
//在通讯录更改指定人信息的函数声明
void change_contact(struct contact* list);
//根据姓名对通讯录排序
void sortbyname(struct contact* list);