#define _CRT_SECURE_NO_WARNINGS 1
#include "contact.h"

void menu()
{
	printf("**************************\n");
	printf("** 1.add    2.del ********\n");
	printf("** 3.search 4.change *****\n");
	printf("** 5.show 6.cleanall *****\n");
	printf("** 7.sort 0.exit**********\n");
}
int main()
{
	int input = 0;
	struct contact list;
	initial_contact(&list);
	do
	{
		menu();
		printf("欢迎打开通讯录，请选择操作\n");
		scanf("%d", &input);
		switch (input)
		{
		case exit:			
			break;
		case add:
			add_contact(&list);
			break;
		case del:
			del_contact(&list);
			break;
		case search:
			search_contact(&list);
			break;
		case change:
			change_contact(&list);
			break;
		case show:
			show_contact(&list);
			break;
		case cleanall:
			initial_contact(&list);
			printf("通讯录已成功清空\n");
			break;
		case sort:
			sortbyname(&list);
			break;
		default:
			printf("输入错误，请重新输入数字\n");
			break;
		}
	} while (input);
	printf("成功退出通讯录，再见\n");
	return 0;
}