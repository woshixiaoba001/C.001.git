#define _CRT_SECURE_NO_WARNINGS 1
#include "stack.h"

using namespace xboll;

int main()
{
	stack<int,list<int>> s1;
	s1.push(1);
	s1.push(2);
	s1.push(3);
	s1.push(4);
	while (!s1.empty())
	{
		cout << s1.top() << " ";
		s1.pop();
	}
	cout << endl;
	return 0;
}