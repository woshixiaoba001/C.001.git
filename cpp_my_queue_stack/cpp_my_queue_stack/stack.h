#pragma once
#include <iostream>
#include <vector>
#include <list>
#include <string>
using namespace std;
namespace xboll
{
	template<class T, class Container = vector<T>>
	class stack
	{
	public:
		void push(const T x)
		{
			obj.push_back(x);
		}

		void pop()
		{
			obj.pop_back();
		}

		T& top()
		{
			return obj.back();
		}

		bool empty()
		{
			return obj.empty();
		}

		size_t size()
		{
			obj.size();
		}

	private:
		Container obj;
	};
}