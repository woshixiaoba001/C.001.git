#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
//
//编写一个函数 long long factorial(int n)，用于计算 n 的阶乘。（要求使用递归实现）
//输入描述：
//键盘输入任意一个整数 n ，范围为 1 - 20
//输出描述：
//输出 n 的阶乘
int long long factorial(int n) {
    if (n == 1)
        return 1;
    else
        return n * factorial(n - 1);
}
int main() {
    int n = 0;
    scanf("%d", &n);
    printf("%lld", factorial(n));
}