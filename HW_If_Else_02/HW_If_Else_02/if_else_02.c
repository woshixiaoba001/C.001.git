#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//计算1 / 1 - 1 / 2 + 1 / 3 - 1 / 4 + 1 / 5 …… + 1 / 99 - 1 / 100 的值，打印出结果
//int main()
//{
//	float sum = 0;
//	int i = 0;
//	int tag = 1;
//	for (i = 1; i <= 100; i++)
//	{
//		sum = sum + (1.0 / i)*tag;
//		tag = tag * (-1);
//	}
//	printf("% lf\n", sum);
//	return 0;
//}

//编写程序数一下 1到 100 的所有整数中出现多少个数字9
//int main()
//{
//	int num = 0;
//	int count = 0;
//	for (num = 1; num <= 100; num++)
//	{
//		if (num % 10 == 9)
//			count++;
//		if (num / 9 == 10)
//			count++;
//	}
//	printf("%d\n", count);
//	return 0;
//}

//猜数字游戏

//#include <math.h>
//void menu()
//{
//	printf("***************************\n");
//	printf("*********  1.play  ********\n");
//	printf("*********  0.exit  ********\n");
//	printf("***************************\n");
//}
//void game()
//{
//	int guess = 0; 
//	int key = rand() % 100 + 1;
//
//	while(guess != key)
//	{
//		printf("please guess number\n");
//		scanf("%d", &guess);
//		if (guess > key)
//			printf("bigger than key\n");
//		else if (guess < key)
//			printf("less than key\n");	
//	}
//	printf("you got me congratulation\n");
//}
//int main()
//{
//	menu();
//	int input = 0;
//	srand((unsigned int)time(NULL));
//	do
//	{
//		printf("please enter 1.play 0.exit\n");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("end\n");
//			break;
//		default:
//			printf("pls enter again\n");
//			break;
//		}
//	} while (input);
//
//	return 0;
//}

//编写代码在一个整形有序数组中查找具体的某个数
//
//要求：找到了就打印数字所在的下标，找不到则输出：找不到。

int main()
{
	int arr[10] = { -100,-23,-2,0,5,80,40,100,121,133 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int left = 0;
	int right = sz - 1;
	int target = 100;
	while (left <= right)
	{
		int mid = left + (right - left) / 2;
		if (arr[mid] < target)
		{
			left = mid + 1;
		}
		else if(arr[mid] > target)
		{
			right = mid - 1;
		}
		else if (arr[mid] == target)
		{
			printf("%d", mid);
			break;
		}
	}
	
	if(left>right)
	printf("not found");
	return 0;
}