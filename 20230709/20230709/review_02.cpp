#include <iostream>
#include <vector>
#include <assert.h>
#include <cstdlib>
using namespace std;



namespace xboll
{
	template<class T>				
	class my_vector
	{
	public:
		typedef T* iterator;

		my_vector()
			:_start(nullptr)
			, _finish(nullptr)
			, _end_of_storage(nullptr)
		{}

		T& operator[](size_t pos)
		{
			assert(pos < size());
 			return *(_start+pos);
		}
		iterator begin()
		{
			return _start;
		}

		iterator end()
		{
			return _finish;
		}
		void reserve(size_t n)
		{
			if (n > capacity())
			{
				size_t sz = size();
				T* tmp = new T[n];
				if (_start)
				{
					memcpy(tmp, _start, size() * sizeof(T));
					delete[] _start;
				}
				_start = tmp;
				_finish = tmp + sz;
				_end_of_storage = tmp + n;
			}
		}

		void push_back(const T& x)
		{
			if (_finish == _end_of_storage)
			{
				reserve(capacity() == 0 ? 3 : capacity() * 2);
			}
			*_finish = x;
			_finish++;
		}
		size_t size()
		{
			return _finish - _start;
		}

		size_t capacity()
		{
			return _end_of_storage - _start;
		}
		
		bool empty()
		{
			return _finish == _start;
		}
	
	private:
		iterator _start;
		iterator _finish;
		iterator _end_of_storage;
	};

}
class Solution {
public:
	vector<vector<int>> generate(int numRows) {
		vector<vector<int>> vv;
		vv.resize(numRows, vector<int>());
		for (size_t i = 0; i < vv.size(); ++i)
		{
			vv[i].resize(i + 1, 0);
			vv[i][0] = vv[i][vv[i].size() - 1] = 1;
		}

		for (size_t i = 0; i < vv.size(); ++i)
		{
			for (size_t j = 0; j < vv[i].size(); ++j)
			{
				if (vv[i][j] == 0)
				{
					vv[i][j] = vv[i - 1][j] + vv[i - 1][j - 1];
				}
			}
		}

		return vv;
	}
};

int main()
{
	//xboll::my_vector<int> v;
	//v.push_back(1);
	////v.push_back(2);
	////v.push_back(3);
 //	for (int i = 0; i < v.size(); i++)
	//{
	//	cout << v[i] << " ";
	//}
	//cout << endl;
	////v.push_back(4);
	////v.push_back(5);
	////v.push_back(5);
	////v.push_back(5);
	////v.push_back(5);
	////v.push_back(5);
	////v.push_back(5);


	////for (auto e : v)
	////{
	////	cout << e << " ";
	////}
	////cout << endl;

	//cout << v.size() << " " << v.capacity()<< " " << v.empty();
	//cout << endl;
	vector<vector<int>> vv1;
	vector<vector<int>> ret = Solution().generate(5);
	vv1.swap(ret);
	for (size_t i = 0; i < vv1.size(); ++i)
	{
		for (size_t j = 0; j < vv1[i].size(); ++j)
		{
			cout << vv1[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;


	return 0;
}