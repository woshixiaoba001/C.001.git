#define _CRT_SECURE_NO_WARNINGS 1
#include "SLlist.h"

int main()
{
	SLN* testsl = NULL;
	testsl = CreateSLlist(0);

	SLlistPushFront(&testsl, 5);
	SLlistPushFront(&testsl, 55);
	SLlistPushFront(&testsl, 555);
	SLlistPushFront(&testsl, 5555);
	SLN* ret1 = SLlistFind(testsl, 5);
	SLN* ret2 = SLlistFind(testsl, 555);
	SLlistInsertAfter(ret1, 999);
	SLlistEraseAfter(ret2);
	SLlistPrint(testsl);
	return 0;
}
