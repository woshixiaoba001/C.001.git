#define _CRT_SECURE_NO_WARNINGS 1
#include "binary_tree.h"
void preorder(BTNode* root);
void test1()
{
	int i = 0;
	char arr[] = "ABD##E#H##CF##G##";
	BTNode* root =  BinaryTreeCreate(arr, sizeof(arr)/sizeof(char), &i);
	BinaryTreePrevOrder(root);
	printf("\n");

	printf("%d \n",BinaryTreeSize(root));
	printf("%d \n", BinaryTreeLeafSize(root));
	printf("%d \n", BinaryTreeLevelKSize(root, 4));
	BinaryTreeLevelOrder(root);
	printf("\n");

	int ret = BinaryTreeComplete(root);
	printf("%d \n", ret);


	

}

int main()
{
	test1();
	return 0;
}