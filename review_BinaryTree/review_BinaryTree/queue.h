#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
#include "binary_tree.h"
typedef BTNode* QueueDatatype;

typedef struct queueNode
{
	QueueDatatype val;
	struct queueNode* next;
}Qnode;

typedef struct Queue
{
	Qnode* head;
	Qnode* tail;
	int size;
}Q;

void Qinitial(Q* pq);
void Qdestroy(Q* pq);

void Qpush(Q* pq, QueueDatatype x);
void Qpop(Q* pq);

QueueDatatype Qfront(Q* pq);
QueueDatatype Qback(Q* pq);

bool Q_isempty(Q* pq);
int Q_size(Q* pq);


