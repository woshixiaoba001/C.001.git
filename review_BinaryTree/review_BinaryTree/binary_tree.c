#define _CRT_SECURE_NO_WARNINGS 1
#include "binary_tree.h"
#include "queue.h"

// 通过前序遍历的数组"ABD##E#H##CF##G##"构建二叉树
BTNode* BinaryTreeCreate(BTDataType* a, int n, int* pi)
{
	//遍历数组时，若遇到#，表示为空树，返回空
	if (*pi< n && a[*pi] == '#')
	{
		(*pi)++;
		return NULL;
	}
	//防止数组遍历时越界
	if (*pi < n)
	{
		//非#即非空节点，为节点开辟空间，并赋值，然后递归继续构建
		BTNode* newnode = (BTNode*)malloc(sizeof(BTNode));
		if (newnode == NULL)
		{
			perror("malloc failed");
			exit(-1);
		}

		newnode->_data = a[(*pi)++];
		/*(*pi)++;*/
		newnode->_left = BinaryTreeCreate(a, n, pi);
		newnode->_right = BinaryTreeCreate(a, n, pi);
		return newnode;
	}
	else
		return NULL;
}

// 二叉树销毁
void BinaryTreeDestory(BTNode** root)
{
	//二叉树的销毁必须是后根顺序来进行
	if ((*root) == NULL)
		return;
	BinaryTreeDestory(&((*root)->_left));
	BinaryTreeDestory(&((*root)->_right));
	free((*root));
	*root = NULL;
}

// 二叉树节点个数
int BinaryTreeSize(BTNode* root)
{
	if (root == NULL)
		return 0;
	return BinaryTreeSize(root->_left) + BinaryTreeSize(root->_right) + 1;
}

// 二叉树叶子节点个数
int BinaryTreeLeafSize(BTNode* root)
{
	if (root == NULL)
		return 0;
	if (root->_left == NULL && root->_right == NULL)
		return 1;
	return BinaryTreeLeafSize(root->_left) + BinaryTreeLeafSize(root->_right);
}

// 二叉树第k层节点个数
int BinaryTreeLevelKSize(BTNode* root, int k)
{
	if (root == NULL)
		return 0;
	if (k == 1)
		return 1;
	if (k > 1)
	{
		return BinaryTreeLevelKSize(root->_left, k - 1) + BinaryTreeLevelKSize(root->_right, k - 1);
	}
	else
		return 0;
}

// 二叉树查找值为x的节点
BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	if (root == NULL)
		return NULL;
	if (root->_data == x)
		return root;
	BTNode* ret1 = BinaryTreeFind(root->_left, x);
	if (ret1)
		return ret1;
	BTNode* ret2 = BinaryTreeFind(root->_right, x);
	if (ret2)
		return ret2;	
	return NULL;
}

// 二叉树前序遍历 
void BinaryTreePrevOrder(BTNode* root)
{
	if (root == NULL)
		return;
	printf("%c ", root->_data);
	BinaryTreePrevOrder(root->_left);
	BinaryTreePrevOrder(root->_right);
}
// 二叉树中序遍历
void BinaryTreeInOrder(BTNode* root)
{
	if (root == NULL)
		return;
	BinaryTreePrevOrder(root->_left);
	printf("%c ", root->_data);
	BinaryTreePrevOrder(root->_right);
}
// 二叉树后序遍历
void BinaryTreePostOrder(BTNode* root)
{
	if (root == NULL)
		return;
	BinaryTreePrevOrder(root->_left);
	BinaryTreePrevOrder(root->_right);
	printf("%c ", root->_data);
}

// 层序遍历
void BinaryTreeLevelOrder(BTNode* root)
{
	Q q1;
	Qinitial(&q1);
	if(root != NULL)
	Qpush(&q1, root);
	while (!Q_isempty(&q1))
	{
		BTNode* front = Qfront(&q1);
		printf("%c ", front->_data);
		Qpop(&q1);
		if(front->_left != NULL)
		Qpush(&q1, front->_left);
		if (front->_right != NULL)
		Qpush(&q1, front->_right);
	}
	Qdestroy(&q1);
	
}

// 判断二叉树是否是完全二叉树
int BinaryTreeComplete(BTNode* root)
{
	Q q1;
	Qinitial(&q1);
	if (root != NULL)
		Qpush(&q1, root);
	//将节点的左右孩子节点指针都放进队列，当得到第一个空指针停下，
	while (!Q_isempty(&q1))
	{
		BTNode* front = Qfront(&q1);
		Qpop(&q1);

		if (front == NULL)
			break;
		else 
		{
			Qpush(&q1, front->_left);
			Qpush(&q1, front->_right);
		}

	}
	//然后检查队列中的指针是否全为空，若是，则为完全二叉树
	while (!Q_isempty(&q1))
	{
		if (Qfront(&q1)!=NULL)
		{
			Qdestroy(&q1);
			return false;
		}
		else
		Qpop(&q1);
	}
	Qdestroy(&q1);
	return true;
}







