#pragma once
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define NAME 20
#define SEX 10
#define TEL 11
#define ADRESS 20
#define MAX 100
#define INITIAL_SIZE 3
#define INC_SIZE 2

//个人信息
struct peopleinfo
{
	char name[NAME];
	int age;
	char sex[SEX];
	char tel[TEL];
	char adress[ADRESS];
};

//静态版本通讯录V1
//struct contact
//{
//	struct peopleinfo PI[MAX];
//	int sz;
//};

//动态版本通讯录V2
struct contact
{
	struct peopleinfo* PI;
	int sz;
	int capacity;
};

//初始化通讯录
void initial_contact(struct contact* pi);
//添加联系人
void add(struct contact* pi);
//打印通讯录
void show(const struct contact* pi);
//删除联系人
void del(struct contact* pi);
//查找指定联系人
int findbyname(struct contact* pi, char name[]);
//修改联系人
void modify(struct contact* pi);
//查找联系人
void search(const struct contact* pi);
//排序通讯录
void sort(struct contact* pi);
//动态通讯录V2释放动态开辟的内存空间
void destory_contact(struct contact* pi);
