#define _CRT_SECURE_NO_WARNINGS 1
#include "contact.h"
void menu()
{
	printf("************************\n");
	printf("***  1.add   2.del  ****\n");
	printf("*** 3.modify 4.search ***\n");
	printf("*** 5.sort  6.show ******\n");
	printf("********* 0.exit ********\n");
}

int main()
{
	int input = 0;
	struct contact contact;

	initial_contact(&contact);
	do
	{
		menu();
		printf("请选择您的操作\n");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			add(&contact);
			break;
		case 2:
			del(&contact);
			break;
		case 3:
			modify(&contact);
			break;
		case 4:
			search(&contact);
			break;
		case 5:
			sort(&contact);
			break;
		case 6:
			show(&contact);
			break;
		case 0:
			destory_contact(&contact);
			printf("成功退出通讯录\n");
			break;
		default:
			printf("输入错误，请重新输入\n");
			break;
		}
	} while (input);

	return 0;
}