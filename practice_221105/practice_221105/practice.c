#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
//牛牛商场促销活动：
//满100打9折；
//
//满500打8折；
//
//满2000打7折；
//满5000打6折
//牛阿姨算不清楚自己应该付多少钱，请你帮忙算一下
//输入描述：
//牛阿姨购买商品打折前的总金额
//输出描述：
//参加活动后，牛阿姨购买商品应付金额。（保留小数点后1位）
int main()
{
    float input = 0;
    float sum = 0;
    scanf("%f", &input);
    if (input >= 100 && input < 500)
        printf("%.1f", sum = 0.9 * input);
    else if (input >= 500 && input < 2000)
        printf("%.1f", sum = 0.8 * input);
    else if (input >= 2000 && input < 5000)
        printf("%.1f", sum = 0.7 * input);
    else if (input >= 5000)
        printf("%.1f", sum = 0.6 * input);
}