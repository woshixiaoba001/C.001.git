#define _CRT_SECURE_NO_WARNINGS 1
//KiKi学习了循环，BoBo老师给他出了一系列打印图案的练习，该任务是打印用“* ”组成的正斜线形图案。
//输入描述：
//多组输入，一个整数（2~20），表示输出的行数，也表示组成正斜线的“ * ”的数量。
//输出描述：
//针对每行输入，输出用“ * ”组成的正斜线。

#include <stdio.h>

int main()
{
    int n = 0;
    while (scanf("%d", &n) != EOF)
    {
        getchar();
        int i = 0;
        int j = 0;
        for (i = 0; i < n; i++)
        {
            for (j = 0; j < n - i; j++)
            {
                if (j == n - i - 1 && j >= 0)
                    printf("*");
                else
                    printf(" ");
            }
            printf("\n");
        }
    }

    return 0;
}