#define _CRT_SECURE_NO_WARNINGS 1
//KiKi学习了循环，BoBo老师给他出了一系列打印图案的练习，该任务是打印用数字组成的数字三角形图案。
//输入描述：
//多组输入，一个整数（3~20），表示数字三角形边的长度，即数字的数量，也表示输出行数。
//输出描述：
//
//针对每行输入，输出用数字组成的对应长度的数字三角形，每个数字后面有一个空格。
#include <stdio.h>

int main()
{
    int n = 0;
    while (scanf("%d", &n) != EOF)
    {
        int i = 0;
        int j = 0;
        int arr[n];
        for (i = 0; i < n; i++)
        {
            arr[i] = i + 1;
        }

        for (i = 0; i < n; i++)
        {
            for (j = 0; j <= i; j++)
            {
                printf("%d ", arr[j]);
            }
            printf("\n");
        }
    }
    return 0;
}