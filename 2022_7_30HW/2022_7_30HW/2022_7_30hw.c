#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//获取一个整数二进制序列中所有的偶数位和奇数位，分别打印出二进制序列
//void cal_oddandeven(int x)
//{
//	int i = 0;
//	int arr[32] = {0};
//	for (i = 0; i < 32; i++)
//	{
//		if (x & 1)
//			arr[i] = 1;
//		else
//			arr[i] = 0;
//		x = x >> 1;
//	}
//	int j = 0;
//	printf("odd is:  ");
//	for (j = 30; j >= 0; j -= 2)
//	{
//		printf("%d ", arr[j]);
//	}
//	printf("\n");
//
//	printf("even is:");
//	for (j = 31; j >= 0; j -= 2)
//	{
//		printf("%d ", arr[j]);
//	}
//}
//
//int main()
//{
//	int input = 0;
//	scanf("%d", &input);
//	cal_oddandeven(input);
//	return 0;
//}
//
//#include <stdio.h>
int cal_gcd(int x, int y)
{
    int ret = 0;
    if (x <= y)
    {
        int i = 0;
        for (i = x; i > 0; i--)
        {
            if (y % i == 0)
            {
                ret = i;
                break;
            }
        }
    }
    else
    {
        int i = 0;
        for (i = y; i > 0; i--)
        {
            if (x % i == 0)
            {
                ret = i;
                break;
            }
        }
    }
    return ret;
}

int cal_lcm(int x, int y, int gcd)
{
    int ret = x * y / gcd;
 
    return ret;
}

int main()
{
    int n = 0;
    int m = 0;
    int gcd = 0;
    int lcm = 0;
    scanf("%d %d", &n, &m);
    gcd = cal_gcd(n, m);
    lcm = cal_lcm(n, m, gcd);
    printf("%d", gcd + lcm);
    return 0;
}