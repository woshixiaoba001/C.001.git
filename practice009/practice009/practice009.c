#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//判断一个整数n是否是闰年
//
//输入描述：
//输入一个整数n （1≤n≤20181≤n≤2018）
//输出描述：
//是闰年输出"yes" 否则输出"no"

//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    if ((n % 4 == 0) && (n % 100 != 0) || n % 400 == 0)
//    {
//        printf("yes\n");
//    }
//    else
//    {
//        printf("no\n");
//    }
//
//    return 0;
//}

//int Add(int x, int y)
//{
//    return x + y;
//}
//
//int main()
//{
//    //pf就是函数指针变量
//    int (*pf)(int x, int y) = &Add;
//    //int (*pf)(int x, int y) = Add;
//    int sum = pf(3, 5);
//    //因为在add 与 &add打印出来的地址相同，
//    //所以推断出既可以写成解引用形式，pf也可以省略*，甚至加很多*，因为并不影响。
// /*   int sum = Add(3, 5);*/
//    printf("%d\n", sum);
//    int arr[10] = { 0 };
//    int(*p)[10] = &arr;
//    printf("%p\n", &arr);//取出数组的地址
//    printf("%p\n", arr);//取出数组的地址
//
//    //Add 和 &Add都是函数的地址，没有区别
//    printf("%p\n", Add);
//    printf("%p\n", &Add);
//    return 0;
//}

//#define num 4
//int add(int x, int y)
//{
//	return x + y;
//}
//
//int sub(int x, int y)
//{
//	return x - y;
//}
//
//int multi(int x, int y)
//{
//	return x * y;
//}
//
//int div(int x, int y)
//{
//	return x / y;
//}
//
//void menu()
//{
//	printf("*********************\n");
//	printf("****** 1.add   ******\n");
//	printf("****** 2.sub   ******\n");
//	printf("****** 3.multi ******\n");
//	printf("****** 4.div   ******\n");
//	printf("*********************\n");
//
//}
//
//int main()
//{
//	int input = -1;
//	int (*arr[num + 1])(int, int) = { 0, add, sub, multi, div };
//	int a = 0;
//	int b = 0;
//	
//	while (input)
//	{
//		menu();
//		printf("请选择计算模式：\n");
//		scanf("%d", &input);
//		getchar();
//		printf("请输入数字：");
//		scanf("%d%d", &a, &b);
//		getchar();
//		if (input >= 1 && input <= num)
//		{
//			printf("%d\n", arr[input](a, b));
//		}
//		else
//			printf("选择错误，请重新选择计算模式\n");
//	}
//		
//	return 0;
//}
#include <string.h>

int main()
{
	char arr[] = "abcdef";

	//char ch = 'w';
	//int* pc = &ch;//char*

	printf("%d\n", strlen(arr));
	printf("%d\n", strlen(arr + 0));
	printf("%d\n", strlen(*arr));
	printf("%d\n", strlen(arr[1]));
	printf("%d\n", strlen(&arr));
	printf("%d\n", strlen(&arr + 1));
	printf("%d\n", strlen(&arr[0] + 1));

	//printf("%d\n", sizeof(arr));
	//printf("%d\n", sizeof(arr + 0));
	//printf("%d\n", sizeof(*arr));
	//printf("%d\n", sizeof(arr[1]));
	//printf("%d\n", sizeof(&arr));
	//printf("%d\n", sizeof(&arr + 1));
	//printf("%d\n", sizeof(&arr[0] + 1));

	return 0;
}

