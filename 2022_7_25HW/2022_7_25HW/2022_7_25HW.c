#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//编写一个函数实现n的k次方，使用递归实现。
//int power_k_n(int x, int y)
//{
//	int r = 0;
//	if (y != 0)
//	{
//		r = x * power_k_n(x, y - 1);
//	}
//	else
//	{
//		return 1;
//	}
//	return r;
//}
//
//int main()
//{
//	int n = 0;
//	int k = 0;
//	int ret = 0;
//
//	printf("pls enter n=");
//	scanf("%d", &n);
//	printf("\npls enter k=");
//	scanf("%d", &k);
//
//	ret = power_k_n(n,k);
//	printf("n power k=%d", ret);
//	return 0;
//}

//写一个递归函数DigitSum(n)，输入一个非负整数，返回组成它的数字之和
//
//例如，调用DigitSum(1729)，则应该返回1 + 7 + 2 + 9，它的和是19
//
//输入：1729，输出：19

//unsigned int DigitSum(unsigned int x)
//{
//	unsigned int q = 0;
//	unsigned int r = 0;
//	
//	r = x % 10;
//	q = x / 10; 
//
//	if (q != 0)	
//	{
//		return DigitSum(q) + r; 
//	}
//	else
//	{
//		return  r;
//	}
//	
//}
//
//int main()
//{
//	unsigned int input = 0;
//	printf("pls enter num=");
//	scanf("%d", &input);
//	
//	printf("%d",DigitSum(input));
//	
//	return 0;
//}

//编写一个函数 reverse_string(char* string)（递归实现）
//
//实现：将参数字符串中的字符反向排列，不是逆序打印。
//
//要求：不能使用C函数库中的字符串操作函数。

char reverse_string(char* arr, int x)
{
	char ret[12] = {'0'};
	if (*arr != '\0')
	{
		*(ret+x) = reverse_string(arr + 1,x-1) + (*arr);
	}
	else
	{
		return '\0';
	}
	return *ret;
}

int main()
{
	int i = 3;
	char input[12] = "abcdefghij";
	reverse_string(input,12);
	return 0;
}

//递归和非递归分别实现strlen
//递归

//int my_strlen(char* arr)
//{
//	int count = 0;
//	
//
//	if (*arr != '\0')
//	{
//		count = my_strlen(arr + 1) + 1;
//	}
//	else
//	{
//		return 0;
//	}
//	return count;
//}
//
//int main()
//{
//	char ch[10] = "abcde";
//	
//	printf("%d",my_strlen(ch));
//
//	return 0;
//}

//非递归
//#include <string.h>
//int my_strlen2(char* arr,int x)
//{
//	int i = 0;
//	int count = 0;
//	for (i = 0; i <= x; i++)
//	{
//		if (*(arr+i) != '\0')
//		{
//			count++;
//		}
//		else
//		{
//			break;
//		}
//	}
//	return count;
//}
//
//int main()
//{
//	char ch[10] = "abcde";
//
//	printf("%d",my_strlen2(ch, 10));
//
//	return 0;
//}

